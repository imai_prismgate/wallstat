<?php
/*
 * @package WordPress
 * @subpackage wallstat
*/

$site_url = home_url();
$theme_url = get_template_directory_uri();
$this_post_type = get_post_type( $post ); //表示されているページの投稿タイプを取得
$ancestor_info = get_ancestor_info($post); //先祖情報取得

?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1>404 Not Found</h1>
		</div>
		<ul class="l-bread"><li>TOP</li><li>404 Not Found</li></ul>
	</div>

	<article class="l-contents">
		<main class="l-main" role="main">
			<p>申し訳ございません。</p>
			<p>お探しのページは見つかりませんでした。</p><br>

			<ul class="c-list_obj_icon" style="margin-left: 15px;">
				<li>URLが間違っている可能性があります。</li>
				<li>一時的にアクセスできない可能性があります。</li>
				<li>ページが移動もしくは削除された可能性があります。</li>
			</ul>
			<br><br><br>

			<p><a href="<?php echo home_url(); ?>">トップに戻る</a></p>
			<p><a href="<?php echo home_url(contact); ?>">お問い合わせ</a></p>

			<!-- Wallstatとはページと子ページのみ表示-->
			<?php if($this_post_type == 'page' && $ancestor_info['slug'] == 'about' ): ?>
				<a  href="<?php echo home_url(); ?>/simulation/" class="p-about_banner">
					<img src="<?php echo $theme_url; ?>/assets/images/p-about_banner.png" width="" height="" alt="Wallstat（ウォールスタット）耐震シュミレーション">
				</a>
			<?php endif; ?>

		</main><!-- l-main END -->

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>
	</article><!-- l-contents END -->

<?php get_footer(); ?>
