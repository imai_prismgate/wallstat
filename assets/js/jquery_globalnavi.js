//ハンバーガーメニュー
$(function() {
    $('.navToggle').click(function() {
        $(this).toggleClass('is-active');

        if ($(this).hasClass('is-active')) {
            $('.l-global_navi_sp').addClass('is-active');
        } else {
            $('.l-global_navi_sp').removeClass('is-active');
        }
    });
});