$(function() {
	//// pagetopボタン制御 ////
	var $topBtn          = $('#js-pageTop'),
		$topBtn_fixStart = 100,	//スクロールが100pxに達したらボタン表示
		$topBtn_fixEnd   = $('.l-footer'),
		$win             = $(window);
	var contentTop = 0; //表示変更をする基準点
	$win.load(function(){
		$topBtn.hide();
		updatePosition();
		update();
	})
	.resize(function(){
		updatePosition();
		update();
	})
	.scroll(function(){
		update();
	});
	// HTMLが動的に変わることを考えて、contentTopを最新の状態に更新
	function updatePosition(){
		contentTop = $topBtn_fixEnd.offset().top + $topBtn.outerHeight();
	}
	// スクロールのたびにチェック
	function update() {
		// 現在のスクロール位置 + 画面の高さで画面下の位置を求める
		if ( $win.scrollTop() + $win.height() < contentTop && $win.scrollTop() > $topBtn_fixStart ) {
			$topBtn.fadeIn().addClass("fixed");
		} else if( $win.scrollTop() <= $topBtn_fixStart ) {
			$topBtn.fadeOut().addClass("fixed");
		} else {
			$topBtn.removeClass("fixed");
		}
		return false;
	}
	$topBtn.click( function () {
		$('body,html').animate({ scrollTop: 0 }, 500);
		return false;
	});
});
