<?php
/*
@package WordPress
@subpackage wallstat
*/

$site_url = home_url();
$theme_url = get_template_directory_uri();
?>

	<footer class="l-footer" role="contentinfo">
		<p id="js-pageTop" class="pageTop"><a href="#top" class="pageTopBtn"></a></p>
		<div class="p-footer_contents">
			<div class="p-footer_left">
				<a href="<?php echo home_url(); ?>" class="p-site_logo"><img src="<?php echo get_template_directory_uri();?>/assets/images/footer_sitelogo.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/footer_sitelogo@2x.png 2x" width="395" height="76" alt="木造軸組住宅用 耐震シミュレーション Wallstat ウォールスタット"></a>
				<p class="catch">※Ｗallstat（ウォールスタット）は木造軸組構法住宅を対象とする数値解析ソフトウェアです。<br />一般社団法人 工務店フォーラム は耐震住宅の普及のためにWallstatの活用と周知啓もう活動を行っています。</p>
			</div>
			<div class="p-footer_right">
				<div class="p-company">
					<p class="name">一般社団法人 工務店フォーラム 耐震研究所</p>
					<p class="add">〒340-0831埼玉県八潮市南後谷480-1【<a href="<?php echo home_url(); ?>/labo/access/">アクセス・地図</a>】</p>
				</div>
				<div class="p-info">
					<span class="tel telLink">TEL 048-951-2420</span><span class="contact">【<a href="<?php echo home_url(); ?>/contact/">お問い合わせ</a>】</span>
					<p class="hour">【営業時間】<span class="br-sp">月〜金 9:00 AM - 5:00 PM　土日  11:00 AM - 3:00 PM</span></p>
				</div>
			</div>
		</div><!-- p-footer_contents END -->

		<div class="p-copyright">
			<small>&copy; <script type="text/javascript">document.write(new Date().getFullYear());</script>  工務店フォーラム</small>
		</div>
	</footer>

</div><!-- l-container END-->

<?php wp_footer(); ?>

<script type="text/javascript">
$(function() {
	// スマホのみ電話リンク
	var ua = navigator.userAgent;
	if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
		$('.telLink').each(function(){
			var str = $(this).text();
			$(this).html($('<a>').attr('href', 'tel:' + str.replace(/-/g, '')).append(str + '</a>'));
		});
	}
	// スマホのみ電話リンク（バナー画像）
	if(ua.indexOf('iPhone') > 0 || ua.indexOf('Android') > 0){
		$('.telLink_img img').each(function(){
			var alt  = $(this).attr("alt");
			var href = 'tel:' + alt.replace(/-/g, '');
			$(this).wrap(
				$('<a></a>').attr({href: href})
			);
		});
	}
	// アンカーリンクの遷移をアニメーション化
	$('a[href^=#]').click(function() {
		// スクロールの速度
		var speed = 400; // ミリ秒
		// アンカーの値取得
		var href= $(this).attr("href");
		// 移動先を取得
		var target = $(href == "#" || href == "" ? 'html' : href);
		// 移動先を数値で取得
		var position = target.offset().top ;
		// スムーススクロール
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
	<?php if ( is_page('contact') || is_page('seminar_entry') || is_page('simulation_entry') ) : ?>
	//// エンターキーで送信されてしまうのを防ぐ
	$(document).on("keypress", "input:not(.allow_submit)", function(event) {
	return event.which !== 13;
	});
	<?php endif; ?>
});
</script>

<!-- 住所自動入力（郵便番号入力欄がひとつ） -->
<?php // if ( is_page('contact')) : ?>
<!-- <script type="text/javascript">
jQuery(function($){
        // 郵便番号入力による住所自動入力
        $('#zipcode').change(function(){
                var zip = $(this).val();
                var url = 'http://api.zipaddress.net?callback=?';
                var query = {'zipcode': zip};
                $.getJSON(url, query, function(json){
                    /* json.pref        都道府県の取得
                     * json.address     市区町村の取得
                     * json.fullAddress 都道府県+市区町村の取得 */
                    $('#address').val(json.fullAddress);
                });
        });
});
</script> -->
<?php // endif; ?>
<!-- 住所自動入力（郵便番号入力欄がふたつ） -->
<?php if ( is_page('contact') || is_page('seminar_entry') || is_page('simulation_entry') ) : ?>
<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>
<script>
$(document).ready(function(){
	jQuery('#zip2').keyup(function(event){
		AjaxZip3.zip2addr('your-zip1','your-zip2','your-address','your-address');
		return false;
	})
})
</script>
<?php endif; ?>

<!-- googlemap制御 -->
<?php if ( is_page('access')) : ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9OR0M7_0ni_01YTjPNqBc8i7f3AapSkw"></script>
<script>
// 地図を挿入したい要素を取得
var canvas = document.getElementById( 'js-googleMap__access' ) ;
// 中心の位置座標を指定
var latlng = new google.maps.LatLng( 35.828657, 139.820526 );
// 地図のオプションを設定する
var mapOptions = {
	zoom: 17,// ズーム値
	center: latlng, // 中心座標 [latlng]
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	scrollwheel: false
};
// [canvas]に、[mapOptions]の内容の、地図のインスタンス([map])を作成する
var map = new google.maps.Map( canvas , mapOptions ) ;
var marker = new google.maps.Marker({
	map: map,//先ほど作った、地図のインスタンス([map]) を設定
	position: latlng//ピンを刺す場所 今回、ピンを挿す位置と地図の中心を同じにしています。
});
//リサイズしてもピンが中心に表示される
google.maps.event.addDomListener(window, 'resize', function(){
	map.panTo(latlng);//地図のインスタンス([map])
});
</script>
<?php endif; ?>

</body>
</html>