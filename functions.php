<?php
/*
@package WordPress
@subpackage wallstat
*/

/*========================================================================
汎用変数・関数など
======================================================================== */
//デバッグ - dabug.logに関数の引数に指定した値を出力する
function _log() {
	if( WP_DEBUG === true ){
		$args = func_get_args();
		foreach ( $args as $arg ) {
			if ( is_string($arg) && $arg !== '' ) {
				error_log($arg);
			} else {
				ob_start();
				var_dump($arg);
				$result = ob_get_clean();
				error_log(trim($result));
			}
		}
	}
}

//曜日を返す
function output_youbi($date) {
	$week  = array ( '日', '月', '火', '水', '木', '金', '土' );
	$youbi = $week[date('w',$date)];
	return $youbi;
}

/*========================================================================
 管理画面カスタマイズ - 全般
======================================================================== */
//管理画面用のCSSの読み込み
function load_admin_costom_css() {
	echo '<link rel="stylesheet" type="text/css" href="'.get_bloginfo('template_directory').'/assets/css/system.css" />';
}
add_action( 'admin_head', 'load_admin_costom_css', 100);

//フッターテキストをオリジナルコメントに変更
function change_admin_footer_text() {
	echo ' お困りの際は<a href="http://prismgate.jp/" target="_blank">プリズムゲート（株）</a>までお問い合わせ下さい。TEL:045-263-8071';
}
add_filter('admin_footer_text', 'change_admin_footer_text');

/*====================================
管理バー
==================================== */
// 管理バーの項目を非表示
function remove_admin_bar_menu( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'wp-logo' ); // WordPressシンボルマーク
	$wp_admin_bar->remove_menu('comments'); // コメント
	$wp_admin_bar->remove_menu('my-account'); // マイアカウント
	$wp_admin_bar->remove_menu('updates'); // 更新
 }
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 70 );

// 管理バーにログアウトを追加
function add_logout_in_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array(
	'id' => 'new_item_in_admin_bar',
	'title' => __('ログアウト'),
	'href' => wp_logout_url()
	));
}
add_action('wp_before_admin_bar_render', 'add_logout_in_admin_bar');

/*====================================
左の操作メニュー
==================================== */
//不必要なメニューを非表示にする
function remove_menu() {
	remove_menu_page('edit.php'); // 投稿
	remove_menu_page('edit-comments.php'); // コメント
	remove_menu_page('link-manager.php'); // リンク
}
add_action('admin_menu', 'remove_menu');

//メニュー名を変更する
add_filter( 'gettext', 'change_side_text' );
add_filter( 'ngettext', 'change_side_text' );
function change_side_text( $translated ) {
	$translated = str_ireplace( 'ダッシュボード', '管理画面TOP', $translated );
	$translated = str_ireplace( 'メディア', '画像・動画の管理', $translated );
	$translated = str_ireplace( '固定ページ', 'ページ', $translated );
	$translated = str_ireplace( '外観', 'デザイン管理', $translated );
	$translated = str_ireplace( 'ユーザー', 'プロフィール設定', $translated );
	return $translated;
}

//メニューの並び替え
function sort_admin_menu($menu_ord) {
	if ( !$menu_ord ) return true;
	return array(
		'index.php', // ダッシュボード
		'separator1', // 最初の区切り線
		'edit.php?post_type=page', // 固定ページ
		'edit.php?post_type=blog', // お知らせ
		'edit.php?post_type=seminar', // セミナー
		'edit.php?post_type=home', // トップ
		'separator2', // 二つ目の区切り線
		'upload.php', // メディア
		'options-general.php', // 設定
		'themes.php', // 外観
		'plugins.php', // プラグイン
		'users.php', // ユーザー
		'tools.php', // ツール
		'separator-last', // 最後の区切り線
	);
}
add_filter('custom_menu_order', 'sort_admin_menu');
add_filter('menu_order', 'sort_admin_menu');

/*====================================
最終更新日を一覧画面のカラムに追加
==================================== */
if ( ! function_exists( 'my_posts_columns' ) AND ! function_exists( 'my_postos_custom_column' ) AND ! function_exists( 'my_posts_orderby_columns' ) AND ! function_exists( 'my_posts_sortable_columns' ) ) {
	// add columns
	function my_posts_columns( $defaults ) {
		$defaults['post_modified'] = __( 'Last updated' );
		return $defaults;
	}
	function my_postos_custom_column( $column_name, $id ) {
		if( $column_name === 'post_modified' ){
			echo get_the_modified_date( 'Y年m月d日' );
		}
	}
	// sort
	function my_posts_orderby_columns( $vars ) {
		if ( isset($vars['orderby']) AND 'post_modified' == $vars['orderby'] ) {
			$vars = array_merge($vars, array(
				'orderby' => 'modified'
			));
		}
		return $vars;
	}
	function my_posts_sortable_columns( $sortable_column ) {
		$sortable_column['post_modified'] = 'post_modified';
		return $sortable_column;
	}
	// sort request
	add_filter( 'request', 'my_posts_orderby_columns' );
	// Page
	add_filter( 'manage_pages_columns', 'my_posts_columns' );
	add_action( 'manage_pages_custom_column', 'my_postos_custom_column', 10, 2 );
	add_filter( 'manage_edit-page_sortable_columns', 'my_posts_sortable_columns' );
}


/*========================================================================
編集画面カスタマイズ - 全般
======================================================================== */
//ビジュアルエディタのスタイルの選択肢を設定
function custom_editor_settings( $initArray ) {
	$initArray['body_class'] = 'editor-area';
	$style_formats = array(
		array(
			'title'   => 'デフォルト',
			'block'   => 'p',
		),
		array(
			'title'   => '見出し2',
			'block'   => 'h2',
			'classes' => 'p-h2'
		),
		array(
			'title'   => '見出し3',
			'block'   => 'h3',
			'classes' => 'p-h3'
		),
		array(
			'title'   => '見出し4',
			'block'   => 'h4',
			'classes' => 'p-h4'
		),
		array(
			'title'   => '見出し1の下のテキスト',
			'block'   => 'p',
			'classes' => 'p-h1_indent'
		),
		array(
			'title'   => '見出し2の下のテキスト',
			'block'   => 'p',
			'classes' => 'p-h2_indent'
		),
		array(
			'title'   => '見出し3の下のテキスト',
			'block'   => 'p',
			'classes' => 'p-h3_indent'
		),
		array(
			'title'   => '見出し4の下のテキスト',
			'block'   => 'p',
			'classes' => 'p-h4_indent'
		),
		array(
			'title'   => 'キャッチコピー',
			'block'   => 'p',
			'classes' => 'p-catchcopy'
		),
		array(
			'title'   => '強調',
			'block'   => 'span',
			'classes' => 'p-em'
		),
		array(
			'title'   => '太字',
			'block'   => 'span',
			'classes' => 'p-bold'
		),
		// array(
		// 	'title'   => 'チェックリスト',
		// 	'block'   => 'span',
		// 	'classes' => 'p-check_txt'
		// ),
		// array(
		// 	'title'   => '※リスト',
		// 	'block'   => 'span',
		// 	'classes' => 'p-kome_txt'
		// ),
	);
	$initArray['style_formats'] = json_encode($style_formats);
	return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );

//ビジュアルエディタ用CSS読み込み
add_editor_style( get_stylesheet_directory_uri().'/assets/css/editor.css' );

//ビジュアルエディタのフォントサイズを増やす
function customize_tinymce_settings($array) {
 $array['fontsize_formats'] = '10px 12px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 36px 40px 48px';
 return $array;
}
add_filter( 'tiny_mce_before_init', 'customize_tinymce_settings' );

//テキストエディタから不要なクイックタグボタンを削除
function default_quicktags($qtInit) {
	$qtInit['buttons'] = 'ol,ul,li,link,img';//表示するボタンのID
	return $qtInit;
}
add_filter('quicktags_settings', 'default_quicktags', 10, 1);

//テキストエディタにクイックタグボタンを追加
function appthemes_add_quicktags() {
	if (wp_script_is('quicktags')){
?>
	<script type="text/javascript">
	QTags.addButton( 'division', 'div', '<div>', '</div>', '', '' , 1 );
	QTags.addButton( 'paragraph', 'p', '<p>', '</p>', '', '', 2 );
	QTags.addButton( 'span', 'span', '<span>', '</span>', '', '', 3);
	QTags.addButton( 'h2', 'h2', '<h2 class="p-h2">', '</h2>', '', '', 4 );
	QTags.addButton( 'h2_text', 'h2_indent', '<p class="p-h2_indent">', '</p>', '', '', 5 );
	QTags.addButton( 'h3', 'h3', '<h3 class="p-h3">', '</h3>', '', '', 6 );
	QTags.addButton( 'h3_text', 'h3_indent', '<p class="p-h3_indent">', '</p>', '', '', 7 );
	QTags.addButton( 'h4', 'h4', '<h4 class="p-h4">', '</h4>', '', '', 8 );
	QTags.addButton( 'h4_text', 'h4_indent', '<p class="p-h4_indent">', '</p>', '', '', 9 );
	// QTags.addButton( 'h5', 'h5', '<h5 class="p-h5">', '</h5>', '', '', 10 );
	// QTags.addButton( 'h5_text', 'h5_text', '<p class="p-h5_indent">', '</p>', '', '', 11 );
	QTags.addButton( 'pp', '空p', '<p>&nbsp;</p>', '', '', '', 101 );
	QTags.addButton( 'catch', 'キャッチ', '<p class="p-catchcopy">', '</p>', '', '', 102);
	QTags.addButton( 'p-em', '強調', '<span class="p-em">', '</span>', '', '', 103 );
	QTags.addButton( 'p-bold', '太字', '<span class="p-bold">', '</span>', '', '', 104 );
	QTags.addButton( 'br', 'br', '<br />', '', '', '', 105 );
	QTags.addButton( 'kome_list', '※リスト', '<ul class="c-list_kome">', '</ul>', '', '', 201 );
	QTags.addButton( 'bullet_list', '・リスト', '<ul class="c-list_bullet">', '</ul>', '', '', 202 );
	QTags.addButton( 'asterisk_list', '＊リスト', '<ul class="c-list_asterisk">', '</ul>', '', '', 203 );
	QTags.addButton( 'icon_list', '■アイコンリスト', '<ul class="c-list_obj_icon">', '</ul>', '', '', 301 );
	// QTags.addButton( 'check_list', '✓アイコンリスト', '<ul class="c-list_unique_icon">', '</ul>', '', '', 302 );
	QTags.addButton( 'number_list', '数字リスト', '<ol>', '</ol>', '', '', 303 );
	</script>
<?php
	}
}
add_action( 'admin_print_footer_scripts', 'appthemes_add_quicktags' );

//改行の時に自動的にPタグが挿入されるのを防ぐ
remove_filter('the_content', 'wpautop');
remove_filter( 'the_excerpt', 'wpautop' );

/*====================================
アイキャッチ（サムネイル）
==================================== */
add_theme_support('post-thumbnails'); //アイキャッチを使用できるようにする

//アイキャッチ→サムネイル用画像に置換
function my_admin_init() {
	add_filter( 'gettext', 'change_thumbnail_title', 10, 3 );
}
function change_thumbnail_title( $translate_text, $text, $domain ) {
	return str_replace( 'アイキャッチ画像', 'ページメイン画像（サムネイル）', $translate_text );
}
add_action( 'admin_init', 'my_admin_init' );

//アイキャッチを好きなサイズに調整する
function image_change_thumbnail() {
	// add_image_size('thumbnail', 130, 100, true );
	// add_image_size( 'ex_mainImg', 689); //施工事例用
	// add_image_size( 'ex_subImg', 168, 117, array('center','center') ); //施工事例用
}
add_action( 'after_setup_theme', 'image_change_thumbnail' );

/*====================================
抜粋
==================================== */
// add_post_type_support( 'page', 'excerpt' ); //抜粋を使用できるようにする

// //記事の内容を抜粋表示した際に文章に続きリンクをつける
// function add_excerpt_more($post) {
// 	return '...';
// }
// add_filter('excerpt_more', 'add_excerpt_more');

// //抜粋文字数を制御
// function change_excerpt_length($length) {
// 	return 40;
// }
// add_filter('excerpt_mblength', 'change_excerpt_length');

// //pタグで囲まれないようにする
// remove_filter('the_excerpt', 'wpautop');

// function my_excerpt($content){
// 	$trimMatch = array("&nbsp;" , "&nbsp" , "&nbs" , "&nb" , "&n" );
// 	return str_replace($trimMatch , "" , $content);
// }
// add_filter('get_the_excerpt', 'my_excerpt');

/*====================================
カスタム投稿
==================================== */
//ページネーション
function pagination($pages = '', $range = 2) {
	$showitems = ($range * 2)+1;//表示するページ数（5ページを表示）
	global $paged;//現在のページ値
	if(empty($paged)) $paged = 1;//デフォルトのページ

	if($pages == '')
	{
	global $wp_query;
	$pages = $wp_query->max_num_pages;//全ページ数を取得
	if(!$pages) {//全ページ数が空の場合は1とする
             $pages = 1;
	}}

	if(1 != $pages)//全ページが1でない場合はページネーションを表示する
	{
		//echo "<div class=\"p-archive_footer \">\n";
		echo "<ul class=\"p-pagenation \">\n";
	//Prev：現在のページ値が1より大きい場合は表示
	if($paged > 1) echo "<li class=\"prev\"><a href='".get_pagenum_link($paged - 1)."'>&#171;</a></li>\n";
	for ($i=1; $i <= $pages; $i++)
	{
	if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
	//三項演算子での条件分岐
		echo ($paged == $i)? "<li class=\"active\">".$i."</li>\n":"<li><a href='".get_pagenum_link($i)."'>".$i."</a></li>\n";
		}
	}
	//Next：総ページ数より現在のページ値が小さい場合は表示
	if ($paged < $pages) echo "<li class=\"next\"><a href=\"".get_pagenum_link($paged + 1)."\">&#187;</a></li>\n";
		echo "</ul>\n";
		//echo "</div>\n";
	}
}

/*====================================
カスタムメニュー
==================================== */
//カスタムメニューを適応
// register_nav_menus( array( 'header_navi' => __( 'ヘッダメニュー' ), ) );
register_nav_menus( array( 'global_navi' => __( 'メインメニュー' ), ) );
// register_nav_menus( array( 'footer_navi' => __( 'フッターメニュー ' ), ) );
// register_nav_menus( array( 'side_navi' => __( 'サイドメニュー' ), ) );

//classの再定義
function nav_redefinition_class( $classes, $item ) {
	//カスタムclassの取得
	$costom_class = array();
	foreach ( (array)$classes as $class_name ) {
		if ( strpos($class_name, 'menu-item') !== false ) { //menu-がつくクラスは除外
			continue;
		} elseif ( strpos($class_name, 'current') !== false ) { //currentがつくクラスにはis-acrhiveを付与
			$costom_class[] = 'is-active';
		} else {
			$costom_class[] = $class_name;
		}
	}
	// //カスタム投稿 blog
	if ( (is_singular( 'blog' ) || is_post_type_archive( 'blog' ) || is_tax( 'blog_category' )) && $item->title == "耐震ニュース＆コラム" ) {
		$costom_class[] = 'is-active';
	//カスタム投稿 seminar
	} elseif ( (is_singular( 'seminar' ) || is_post_type_archive( 'seminar' ) || is_tax( 'seminar_category' )) && $item->title == "イベント・セミナー" ) {
		$costom_class[] = 'is-active';
	// //カスタム投稿 interview
	// } elseif ( (is_singular( 'interview' ) || is_post_type_archive( 'interview' ) ) && $item->title == "採用情報" ) {
	// 	$costom_class[] = 'is-active';
	}
	return $costom_class;
}
add_filter( 'nav_menu_css_class', 'nav_redefinition_class', 10, 2 );

//アンカーリンクを指定したいリンクはアンカー付きで出力
// $class_names = '';
// class AnchorlinkNav_Walker extends Walker_Nav_Menu {
// 	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
// 		global $wp_query;
// 		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

// 		$class_names = $value = '';

// 		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

// 		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
// 		$class_names = ' class="' . esc_attr( $class_names ) . '"';

// 		//説明の入力内容によって説明の入力内容出力場所をカスタマイズ
// 		$anchorlink = '';
// 		if ( substr($item->description, 0, 1) == '#' ) { //冒頭の文字が#nの場合はページ内リンク
// 			$anchorlink = $item->description;
// 		}

// 		$output .= $indent . '<li ' . $value . $class_names .'>';

// 		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
// 		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
// 		$attributes .= ! empty( $item->xfn )		? ' rel="'    . esc_attr( $item->xfn		) .'"' : '';
// 		$attributes .= ! empty( $item->url )		? ' href="'   . esc_attr( $item->url.$anchorlink ) .'"' : '';

// 		$item_output = $args->before;
// 		$item_output .= '<a'. $attributes .'>';
// 		$item_output .= $item->title;
// 		$item_output .= '</a>';
// 		$item_output .= $args->after;

// 		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id);
// 	}
// }

/*====================================
TinyMCE Template
==================================== */
//wysiwyg editor of the ACF
add_filter( 'tinymce_templates_enable_media_buttons', 'tinymce_templates_visible_btn' );
function tinymce_templates_visible_btn() {
	return true;
}

/*====================================
WordPress絵文字機能を非表示
==================================== */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/*====================================
非公開・下書き記事を親にできるようにする
==================================== */
add_filter('page_attributes_dropdown_pages_args', 'add_private_draft');
function add_private_draft($args) {
	$args['post_status'] = 'publish,private,draft';
	return $args;
}

/*====================================
タイトル入力欄のプレースホルダーを変更
==================================== */
function change_title_text_all( $title ){
    return $title = 'タイトルを入力してください';
}
add_filter( 'enter_title_here', 'change_title_text_all' );

/*========================================================================
ページ・投稿HTML出力時カスタマイズ
======================================================================== */
//タイトルタグの出力を適応
add_theme_support( 'title-tag' );

//必要ないheadタグを削除
remove_action( 'wp_head', 'feed_links', 2 ); //サイト全体のフィード
remove_action( 'wp_head', 'feed_links_extra', 3 ); //その他のフィード
remove_action( 'wp_head', 'rsd_link' ); //Really Simple Discoveryリンク
remove_action( 'wp_head', 'wlwmanifest_link' ); //Windows Live Writerリンク
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); //前後の記事リンク
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 ); //ショートリンク
remove_action( 'wp_head', 'wp_generator' ); //WPバージョン

//出力時にimgタグの余計な属性を削除
function remove_imgtag_attribute( $html ){
	$html = preg_replace( '/title=[\'"]([^\'"]+)[\'"]/i', '', $html );
	$html = preg_replace( '/<a href=".+">/', '', $html );
	$html = preg_replace( '/<\/a>/', '', $html );
	return $html;
}
add_filter( 'image_send_to_editor', 'remove_imgtag_attribute', 10 );
add_filter( 'post_thumbnail_html', 'remove_imgtag_attribute', 10 );

/*====================================
CSS・JS
==================================== */
//CSS・JSにバージョン情報が付随するのを削除
function remove_cssjs_ver( $src ) {
	if( strpos( $src, '?ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );

//JS migrate 読み込みしない
add_filter( 'wp_default_scripts', 'dequeue_jquery_migrate' );
	function dequeue_jquery_migrate( $scripts){
	if(!is_admin()){
		$scripts->remove( 'jquery');
	}
}

//JSの読み込みを制御
function load_costom_js() {
	if ( !is_admin() ) {
		global $post;
		$theme_url = get_template_directory_uri();
		$ancestors = get_ancestor_info($post);
		if( !is_admin() ) {
			wp_deregister_script( 'jquery' );
			wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js');
			wp_enqueue_script( 'js-pagetop', $theme_url.'/assets/js/jquery_pagetop.js', array('jquery'), '', true ); //ページトップ
			wp_enqueue_script( 'js-globalnavi', $theme_url.'/assets/js/jquery_globalnavi.js', array('jquery'), '', true ); //グローバルナビ制御
			// wp_enqueue_script( 'migrate', '//code.jquery.com/jquery-migrate-1.2.1.min.js');
			// wp_enqueue_script( 'js-changeimg', $theme_url.'/assets/js/change_img.js', array('jquery'), '', true ); //画像チェンジ
			// if ( is_home() || is_front_page() ) {
			// 	wp_enqueue_script( 'swiper', $theme_url.'/assets/js/swiper.min.js'); //スライダー
			// }
		}
	}
}
add_action('wp_enqueue_scripts','load_costom_js');

//CSSの読み込みを制御
if (!function_exists('template_common')) {
	function load_costom_css() {
		$theme_url = get_template_directory_uri();
		// if ( is_singular('works') ) {
		// 	wp_enqueue_style('slick', $theme_url.'/assets/css/slick.css');
		// 	wp_enqueue_style('slick_theme', $theme_url.'/assets/css/slick-theme.css');
		// }//スライダーCSS
		if ( !is_admin() ) {
			wp_enqueue_style('style', $theme_url.'/assets/css/style.css');
			wp_enqueue_style('revision', $theme_url.'/css-revision.css'); //更新用CSS格納用
			// if ( is_home() || is_front_page() ) {
			// 	wp_enqueue_style('swiper', $theme_url.'/assets/css/swiper.min.css'); //スライドショー
			// }
		}
	}
}
add_action('wp_print_styles', 'load_costom_css');

/*====================================
 投稿者アーカイブページへのアクセスを404ページへリダイレクト
==================================== */
if ( !is_admin() ) {
	function author_archive_redirect() {
		if ( isset( $_GET['author'] ) && $_GET['author'] !== null ) {
			wp_redirect( home_url('/404') );
			exit;
		}
	}
	add_action('init', 'author_archive_redirect');
}

/*====================================
 ユーザーエージェント制御（ベンダーのライブラリー使用）
 参考：https://webkikaku.co.jp/blog/wordpress/mobiledetect/
==================================== */
//スマホ
function sp_contents( $atts, $content = null ) {
	require_once('inc/vender/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if( $detect->isMobile() && !$detect->isTablet() ) {
			return '' . $content . '';
	} else {
			return '';
	}
}
add_shortcode('sp-only', 'sp_contents');

//タブレット
function tab_contents( $atts, $content = null ) {
	require_once('inc/vender/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if( $detect->isTablet() ) {
			return '' . $content . '';
	} else {
			return '';
	}
}
add_shortcode('tab-only', 'tab_contents');

//PC
function pc_contents( $atts, $content = null ){
	require_once('inc/vender/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if( !$detect->isMobile() && !$detect->isTablet() ) {
			return '' . $content . '';
	 } else {
			return '';
	 }
	}
add_shortcode('pc-only', 'pc_contents');

//スマホ以外
function not_sp_contents( $atts, $content = null ) {
	require_once('inc/vender/Mobile_Detect.php');
	$detect = new Mobile_Detect;
	if( !$detect->isMobile() ) {
			return '' . $content . '';
	} else {
			return '';
	}
}
add_shortcode('not-sp', 'not_sp_contents');

/*====================================
 googlemap表示用のdivを出力するショートコード
==================================== */
function view_googlemap($atts) {
	extract(shortcode_atts(array(
		'id' => 'js-googleMap__access',
	), $atts));
	return '<div class="c-googlemap_container"><div id="'.$id.'" class="c-googlemap_container__obj"></div></div>';
}
add_shortcode('view_googlemap', 'view_googlemap');

/*========================================================================
外部ファイル読み込み - 汎用関数・各カスタム投稿タイプ情報・関数など
======================================================================== */
//汎用関数の読み込み
foreach(glob(TEMPLATEPATH."/inc/common/*.php") as $file){
	require_once $file;
}

//カスタム投稿の宣言、カスタム投稿内で挙動する関数などの読み込み
$func_posttype = array( 'home', 'blog', 'seminar');
foreach ($func_posttype as $type) {
	get_template_part( 'inc/custom_post/posttype', $type );
}

//ログイン画面のカスタマイズ（デフォルトでチェックを入れる など）
function change_login() {
	$dir = get_template_directory_uri();
	$jq  = 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';
	wp_enqueue_script( 'change_login_jquery', $jq );
	wp_enqueue_script( 'change_login_script', $dir.'/assets/js/script_login.js' );
}
add_action('login_head', 'change_login');

//カスタム投稿のカテゴリの１つ目に強制的にチェックを入れる
function admin_print_footer_scripts_custom() {
    echo '<script type="text/javascript">
      //<![CDATA[
      jQuery(document).ready(function($){
          // default check
          if ($(".categorychecklist input[type=checkbox]:checked").length == 0) {
            $(".categorychecklist li:first-child input:first-child").attr("checked", "checked");
          }
      });
      //]]>
      </script>';
}
add_action('admin_print_footer_scripts', 'admin_print_footer_scripts_custom', 21);

?>
