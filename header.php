<?php
/*
@package WordPress
@subpackage wallstat
 */

$site_url      = home_url();
$theme_url     = get_template_directory_uri();
$ancestor_info = get_ancestor_info($post);

if( is_home() || is_front_page() ) {
	$logo_tag   = 'h1';
	$body_class = 'home';
} else {
	$logo_tag   = 'div';
	$body_class = $ancestor_info['slug'];
}

require_once('inc/vender/Mobile_Detect.php'); //ユーザーエージェント判断
$detect = new Mobile_Detect;

?>

 <!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=Edge"><!-- IEレンダリングモード -->
<meta name="format-detection" content="telephone=no"><!-- 電話番号リンク無効 -->
<link rel="shortcut icon" href="<?php echo $theme_url; ?>/assets/images/system/favicon.ico" type="image/vnd.microsoft.ico"/>
<link rel="icon" href="<?php echo $theme_url; ?>/assets/images/system/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="apple-touch-icon" href="<?php echo $theme_url; ?>/assets/images/system/apple-touch-icon.png" type="image/png">
<link href="https://fonts.googleapis.com/earlyaccess/notosansjp.css" rel="stylesheet" />
<!-- fontAwesome ver.4 -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.7.0/css/font-awesome.css">
<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117497160-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122077900-1');
</script> -->

</head>

<body class="l-page_<?php echo $body_class; ?>">
<div class="l-container">

	<header class="l-header" role="banner">
		<div class="p-header_contents">

			<div class="p-header_left">
				<<?php echo $logo_tag; ?> class="p-site_logo"><a href="<?php echo home_url(); ?>">
					<?php if( $detect->isMobile() && !$detect->isTablet() ) : //スマホのみ ?>
					<img src="<?php echo get_template_directory_uri();?>/assets/images/header_sitelogo.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/header_sitelogo@2x.png 2x" width="603" height="104" alt="本当に「地震に強い家？」耐震シミュレーション Wallstat ウォールスタットで一目瞭然！">
					<?php else: //スマホ以外 ?>
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/header_sitelogo.png" width="603" height="104" alt="本当に「地震に強い家？」耐震シミュレーション Wallstat ウォールスタットで一目瞭然！">
					<?php endif; ?>
				</a></<?php echo $logo_tag; ?>>
			</div><!-- p-header_left END -->
			<div class="p-header_right">
				<a class="p-banner" href="<?php echo home_url(); ?>/simulation/simulation_entry/">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/header_banner.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/header_banner@2x.png 2x" width="230" height="63" alt="耐震シミュレーションお申込">
				</a>
			</div><!-- p-header_right END -->

		</div><!-- p-header_contents END -->

		<!-- globalNavi PC -->
		<?php wp_nav_menu( array(
			'container' => 'nav',
			'container_class' => 'l-global_navi',
			'menu_class' => 'c-global_navi_menu',
			'fallback_cb' => '',
			'theme_location' => 'global_navi',
			'depth' => '1',
			'echo' => '1'
			) );
		?>

		<!-- globalNavi SP -->
		<div class="l-global_navi_sp_wrapper">
			<div class="navToggle">
				<span></span><span></span><span></span>
			</div>
			<?php wp_nav_menu( array(
				'container' => 'nav',
				'container_class' => 'l-global_navi_sp',
				'menu_class' => 'c-global_navi_menu_sp',
				'theme_location' => 'global_navi',
				'fallback_cb' => '',
				'depth' => '1',
				'echo' => '1'
				) );
			?>
		</div>

		<!-- globalNavi END -->
	</header>
