<?php
/*
 * @package WordPress
 * @subpackage wallstat
*/

$site_url  = home_url();
$theme_url = get_template_directory_uri();

?>

<?php get_header(); ?>

	<article class="l-contents  p-home_contents">
		<main class="l-main p-home_main" role="main">
			<?php echo output_home_parts_normal('contents'); ?>
		</main><!-- l-main END -->
	</article><!-- l-contents END -->

<?php get_footer(); ?>