<?php
//指定した投稿の各種情報を取得するための関数です

/*====================================
指定した投稿の先祖情報を取得（主に表示ページの先祖情報取得に使用
==================================== */
function get_ancestor_info($post) {
	$ancestor_info = array();
	$post_type     = esc_html(get_query_var( 'post_type' )); //投稿タイプ名取得
	if ( is_tax() && !$post_type ) {
		$taxonomy  = get_query_var('taxonomy');
		$post_type = get_taxonomy($taxonomy)->object_type[0];
	}
	//固定ページの場合
	if ( is_page() ) {
		$get_post_ancestors     = get_post_ancestors( $post->ID );
		$ancestor_info['id']    = array_pop( $get_post_ancestors ); //先祖IDを取得、先祖がない場合は空
		$ancestor_info['title'] = get_the_title($ancestor_info['id']); //先祖ページタイトルを取得
		$ancestor_info['slug']  = get_page_uri($ancestor_info['id']); //先祖ページスラッグを取得
		$ancestor_info['url']   = get_permalink($ancestor_info['id']); // 先祖ページURLを取得
	}
	//カスタム投稿のアーカイブの場合、カスタム投稿の個別ページの場合
	else if( is_post_type_archive() || is_tax() || ( is_single() && $post_type != 'post' ) ) {
		$posttype_label = esc_html(get_post_type_object( $post_type )->labels->name);
		$archive_url    = esc_html(get_post_type_archive_link($post_type));
		$ancestor_info['title'] = ( $posttype_label ) ? $posttype_label : '';
		$ancestor_info['slug']  = ( $post_type ) ? $post_type : '';
		$ancestor_info['url']   = home_url().'/'.$ancestor_info['slug'];
	}
	// //カスタム投稿の個別ページの場合
	// else if( is_single() ) {
	// 	$post_type = get_post_type( $post );
	// 	if( $post_type != 'post' ){
	// 	 	$ancestor_info['title'] = esc_html( get_post_type_object( get_post_type() )->label );
	// 		$ancestor_info['slug']  = esc_html( get_post_type_object( get_post_type() )->name );
	// 		$ancestor_info['url']   = get_option('home').'/'.$ancestor_info['slug'];
	// 	}
	// }
	else{
		$ancestor_info['title'] = '';
		$ancestor_info['slug']  = '';
		$ancestor_info['url']   = '';
	}
	return $ancestor_info;
}

 ?>
