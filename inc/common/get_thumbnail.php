<?php
//postidからサムネイル生成（表示優先順位：アイキャッチ→記事にある一番最初の画像→指定したダミー画像）
function create_thumbnail($post_item, $dummyimg, $size = 'thumbnail') {
	$theme_url      = get_template_directory_uri();
	$post_thumbnail = get_the_post_thumbnail( $post_item->ID, $size );
	ob_start();
	ob_end_clean();
	preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_item->post_content, $matches);
	$first_img = ( count($matches [1]) >=  1 ) ? $matches [1][0] : '';
	if ( $post_thumbnail ) {
		$thumbnail = $post_thumbnail;
	} elseif ( $first_img != '' ) {
		$thumbnail = '<img src="'.$first_img.'" />';
	} elseif ( $dummyimg != '' ) {
		$thumbnail = '<img src="'.$dummyimg.'" />';
	} else {
		$thumbnail = '<img src="'.$theme_url.'/assets/images/thumbnail_dummy.png" />';
	}
	return $thumbnail;
}

//サムネイル用の画像のパスのみ出力（background-imageで使用）
function create_thumbnail_path($post_item, $dummyimg) {
	$theme_url      = get_template_directory_uri();
	$post_thumbnail = wp_get_attachment_url( get_post_thumbnail_id($post_item->ID) );
	ob_start();
	ob_end_clean();
	preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post_item->post_content, $matches);
	$first_img = ( count($matches [1]) >=  1 ) ? $matches [1][0] : '';
	if ( $post_thumbnail ) {
		return $post_thumbnail;
	} elseif ( $first_img != '' ) {
		return $first_img;
	} elseif ( $dummyimg != '' ) {
		return $dummyimg;
	} else {
		return $theme_url.'/assets/images/thumbnail_dummy.png';
	}
}

?>