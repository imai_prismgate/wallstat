<?php
//パンくずを生成し返します（要引数）
function output_breadcrumb( $post, $home_name = '' ) {
	// global $wp_query;
	$site_url = home_url();
	$ancestor_info = get_ancestor_info( $post ); //先祖情報取得
	$post_type     = get_post_type( $post );
	if ( !is_home() ) {
		//Start Ul
		$html_tag = '<ul class="l-bread"><li><a href="'. home_url() .'">'.$home_name.'</a></li>';
		//アーカイブの場合
		if ( is_post_type_archive() ) {
			//日付アーカイブの場合
			if ( is_month() || is_year() ) {
				$html_tag .= '<li><a href="'.$ancestor_info['url'].'">'.$ancestor_info['title'].'</a></li>';
				$html_tag .= '<li>記事一覧</li>';
			} else {
				$html_tag .= '<li>'.$ancestor_info['title'].'</li>';
			}
		//カスタム投稿タイプの個別ページの場合
		} elseif( $post_type != 'post' && is_single() ) {
			$html_tag .= '<li><a href="'.$ancestor_info['url'].'">'.$ancestor_info['title'].'</a></li>';
			$html_tag .= '<li>'.the_title('','', FALSE).'</li>';
		//投稿タイプの個別ページの場合
		} elseif ( is_single() ) {
			$category    = get_the_category();
			$category_id = get_cat_ID( $category[0]->cat_name );
			$html_tag   .= '<li>'. get_category_parents( $category_id, TRUE, ' &#62; ' ) . the_title('','', FALSE) .'</li>';
		//タクソノミーアーカイブの場合
		} elseif( is_tax() ) {
			$html_tag .= '<li><a href="'.$ancestor_info['url'].'">'.$ancestor_info['title'].'</a></li>';
			//$html_tag .= '<li><a href="'.$site_url.'/news/">撮影レシピ・お知らせ</a></li>';
			$html_tag .= '<li>'.single_term_title('', false).'</li>';
		//カテゴリーアーカイブの場合
		} elseif ( is_category() ) {
			$catTitle  = single_cat_title( "", false );
			$cat       = get_cat_ID( $catTitle );
			$html_tag .= '<li> &raquo;' . get_category_parents( $cat, TRUE, ' &#62; ' ) .'</li>';
		//アーカイブの場合
		} elseif ( is_archive() && !is_category() ) {
			$html_tag .= '<li>Archives</li>';
		//検索結果の場合
		} elseif ( is_search() ) {
			$html_tag .= '<li>検索結果</li>';
		//404エラーの場合
		} elseif ( is_404() ) {
			$html_tag .= '<li>404 Not Found</li>';
		//固定ページの場合
		} elseif ( is_page() ) {
			//親ページの場合
			if ( $post->post_parent == 0 ){
				$html_tag .= '<li>'.the_title('','', FALSE).'</li>';
			//子ページの場合
			} else {
				$title     = the_title('','', FALSE);
				$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
				array_push($ancestors, $post->ID);
				foreach ( $ancestors as $ancestor ) {
					if( $ancestor != end($ancestors) ) {
						$html_tag .= '<li><a href="'. get_permalink($ancestor) .'">'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</a></li>';
					} else {
						$html_tag .= '<li>'. strip_tags( apply_filters( 'single_post_title', get_the_title( $ancestor ) ) ) .'</li>';
					}
				}
			}
		}
		$html_tag .= "</ul>\n";
	}
	return $html_tag;
}
 ?>