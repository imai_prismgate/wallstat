<?php
//指定ID・スラッグの子ページリストを返します（指定がない場合は表示ページの子ページリスト）
function output_main_childpages($atts) {
	extract(shortcode_atts(array(
		'post_id'   => '',
		'post_slug' => '',
	), $atts));
	//引数からターゲットのIDを指定
	if ( $post_slug ) {
		$target_id = get_page_by_path( $post_slug );
		$target_id = $target_id->ID;
	} else {
		$target_id = $post_id;
	}
	//引数からIDが取得できなかった場合は表示ページIDを指定
	if ( !$target_id ) {
		global $post;
		$target_id = $post->ID;
	}
	//リストを生成
	$roop_tag = '';
	$args     = array(
		'posts_per_page' => -1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_type'      => 'page',
		'post_parent'    => $target_id,
	);
	$childpage_list = get_posts($args);
	foreach ($childpage_list as $list) {
		$active_tag = ( $list->ID == $target_id ) ? ' class="is-active"' : '';
		$roop_tag  .= '<li'.$active_tag.'><a href="'.get_permalink($list->ID).'">'."\n";
		// $roop_tag  .= '<p class="thumb">'.create_thumbnail($list,get_template_directory_uri().'/assets/images/thumbnail_dummy.png').'</p>'."\n";
		$thumb_path = create_thumbnail_path($list, get_template_directory_uri().'/images/thumbnail_reform_dummy.png');
		$roop_tag   .= '<p class="thumb" style="background-image:url('.$thumb_path.')"></p>'."\n";
		$roop_tag  .= '<p class="title">'.get_the_title($list->ID).'</p>'."\n";
		$roop_tag  .= '</a></li>'."\n";
	}
	wp_reset_postdata();
	if ( $roop_tag ) {
		return '<div class="p-child_pages__container_thumb"><ul class="p-child_pages__list">'.$roop_tag.'</ul></div>';
	}
}
add_shortcode('output_main_childpages','output_main_childpages');

//サムネイルがないバージョン
function output_main_childpages_textlist($atts) {
	extract(shortcode_atts(array(
		'post_id'   => '',
		'post_slug' => '',
	), $atts));
	//引数からターゲットのIDを指定
	if ( $post_slug ) {
		$target_id = get_page_by_path( $post_slug );
		$target_id = $target_id->ID;
	} else {
		$target_id = $post_id;
	}
	//引数からIDが取得できなかった場合は表示ページIDを指定
	if ( !$target_id ) {
		global $post;
		$target_id = $post->ID;
	}
	//リストを生成
	$roop_tag = '';
	$args     = array(
		'posts_per_page' => -1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_type'      => 'page',
		'post_parent'    => $target_id,
	);
	$childpage_list = get_posts($args);
	foreach ($childpage_list as $list) {
		$active_tag = ( $list->ID == $target_id ) ? ' class="is-active"' : '';
		$roop_tag  .= '<li'.$active_tag.'><a href="'.get_permalink($list->ID).'">'."\n";
		$roop_tag  .= get_the_title($list->ID)."\n";
		$roop_tag  .= '</a></li>'."\n";
	}
	wp_reset_postdata();
	if ( $roop_tag ) {
		return '<div class="p-child_pages__container_text"><ul class="p-child_pages__list">'.$roop_tag.'</ul></div>';
	}
}
add_shortcode('output_main_childpages_textlist','output_main_childpages_textlist');

//サイドバー表示用_固定ページ
function output_main_childpages_sidebar($atts) {
	extract(shortcode_atts(array(
		'post_id'   => '',
		'post_slug' => '',
	), $atts));
	//引数からターゲットのIDを指定
	if ( $post_slug ) {
		$target_id = get_page_by_path( $post_slug );
		$target_id = $target_id->ID;
	} else {
		$target_id = $post_id;
	}
	//引数からIDが取得できなかった場合は表示ページIDを指定
	if ( !$target_id ) {
		global $post;
		$target_id = $post->ID;
	}
	//リストを生成
	$roop_tag = '';
	$args     = array(
		'posts_per_page' => -1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_type'      => 'page',
		'post_parent'    => $target_id,
	);
	$childpage_list = get_posts($args);
	foreach ($childpage_list as $list) {
		$active_tag = ( $list->ID == $target_id ) ? ' class="is-active"' : '';
		$roop_tag  .= '<li'.$active_tag.'><a href="'.get_permalink($list->ID).'">'."\n";
		$roop_tag  .= get_the_title($list->ID)."\n";
		$roop_tag  .= '</a></li>'."\n";
	}
	wp_reset_postdata();
	if ( $roop_tag ) {
		return '<ul class="page_list">'.$roop_tag.'</ul>';
	}
}
add_shortcode('output_main_childpages_sidebar','output_main_childpages_sidebar');

?>