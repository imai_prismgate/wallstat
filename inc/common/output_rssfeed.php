<?php 
//RSSを取得し最新記事情報を取得

function output_rssfeed($atts){
	extract(shortcode_atts(array(
		'num'         => 5,
		'url'         => '',
		'date_format' => 'Y.m.d',
		'class'       => '',
		'text_max'    => 36,
	), $atts));
	include_once(ABSPATH . WPINC . '/feed.php');
	$rss  = fetch_feed($url);
	$i = 0;
	if( !is_wp_error($rss) ) {
		$maxitems  = $rss->get_item_quantity($num + 5); //PR回避のため多めに取得
		$rss_items = $rss->get_items(0, $maxitems);
	}
	$feed = '<ul class="rssFeed '.$class.'">';
	if( $maxitems == 0 ) {
		$feed     .= '<li>更新情報はありません</li>';
	} else {
		foreach ( $rss_items as $item ){
			if( $i++ == $num ) { break; }
			$post_url   = $item->get_permalink();
			$post_title = mb_strimwidth($item->get_title(), 0, $text_max, "…","UTF-8"); //長いタイトルを省略
			$post_date  = $item->get_date($date_format);
			if ( mb_strpos($post_title,'PR') === FALSE ) { //PRタイトルを回避
				$feed  .= '<li><span class="rssList_date">'.$post_date.'</span><span class="rssList_title"><a href="'.$post_url.'" target="_blank">'.$post_title.'</a></span></li>';
			} else {
				$i     = $i - 1;
			}
		}
	}
	$feed .= '</ul>';
	return $feed;
}
add_shortcode('output_rssfeed', 'output_rssfeed');

 ?>