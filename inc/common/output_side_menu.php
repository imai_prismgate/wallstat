<?php
//サイドに表示するメニューリスト//

//固定ページサイド用 - 表示ページの先祖に所属する子ページ・孫ページ一覧を返します
function output_side_childpages($post,$title_flg) {
	$roop_tag           = '';
	//ログイン状態で表示する投稿ステータスを指定
	$post_status        = ( is_user_logged_in() ) ? 'any' : 'publish'; //ログイン中の場合は下書き・非公開を含み取得、それ以外は公開のみ取得
	//現在表示しているページの先祖IDを取得
	$post_ancestors     = get_post_ancestors( $post->ID ); //先祖がない場合はnull
	$post_ancestors     = $post_ancestors ? array_reverse($post_ancestors) : $post_ancestors; //先祖から順になるように配列をリバース
	$ancestor_id        = ( count($post_ancestors) >= 1 ) ? $post_ancestors[0] : $post->ID; //先祖IDを指定
	//先祖タイトルを設定 $title_flgがtrueの場合タイトルが表示される
	$ancestor_title_tag = ( $title_flg == 'true' ) ? '<p class="c-child_list_title"><a href="'.get_permalink($ancestor_id).'">'.get_the_title($ancestor_id).'</a></p>' : '';
	//子ページ取得
	$args = array(
		'posts_per_page' => -1,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'post_type'      => 'page',
		'post_parent'    => $ancestor_id,
		'post_status'    => $post_status
	);
	$childpage_list = get_posts($args);
	foreach ($childpage_list as $list) {
		$active_tag = ( $list->ID == $post->ID || $list->ID == $post->post_parent ) ? ' class="is-active"' : '';
		$roop_tag  .= '<li'.$active_tag.'><a href="'.get_permalink($list->ID).'"><span>'.get_the_title($list->ID).'</span></a>';

		//孫ページ取得
		if ( $active_tag ) {
			$args['post_parent'] = $list->ID;
			$grandchildpage_list = get_posts($args);
			if ( $grandchildpage_list ) {
				$roop_tag .= '<ul class="c-grandchild_list">';
				foreach ($grandchildpage_list as $grandchild) {
					$active_tag = ( $grandchild->ID == $post->ID ) ? ' class="is-active"' : '';
					$roop_tag  .= '<li'.$active_tag.'><a href="'.get_permalink($grandchild->ID).'">'.get_the_title($grandchild->ID).'</a></li>';
				}
				$roop_tag .= '</ul>';
			}
		}
		$roop_tag .= '</li>';
	}
	if ( $roop_tag ) {
		return '<div class="c-child_list_container">'.$ancestor_title_tag.'<ul class="c-child_list">'.$roop_tag.'</ul></div>';
	}
}

//最新の記事リスト - 日付ソート形式（日付表示は任意でフラグ設定）
function side_archives_sort_date( $list_title = NULL, $post_type = NULL, $num = 5, $date_visible = 'true' ) {
	$title_tag = ( $list_title ) ? '<p>'.$list_title.'</p>' : '';
	$html_tag  = '';
	$args      = array(
		'post_type'      => $post_type,
		'orderby'        => 'date',
		'order'          => 'DESC',
		'posts_per_page' => $num,
	);
	$archives = get_posts($args);
	foreach ( $archives as $post ) {
		$date      = ( $date_visible == 'true' ) ? '<span class="c-sidebar_post_date">'.get_the_time('Y.m.d', $post->ID).'</span>' : '';
		$html_tag .= '<li><a href="'.get_permalink($post->ID).'">'.$date.'<span class="c-sidebar_post_title">'.get_the_title($post->ID).'</span>'.'</a></li>';
	}
	if ( $html_tag ) {
		return '<div class="l-sidebar_post_list is-new">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}

//最新の記事リスト - ページNoソート形式
function side_archives_sort_pageno( $list_title = NULL, $post_type = NULL, $num = 5 ) {
	$title_tag = ( $list_title ) ? '<p>'.$list_title.'</p>' : '';
	$html_tag  = '';
	$args      = array(
		'post_type'      => $post_type,
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'posts_per_page' => $num,
	);
	$archives = get_posts($args);
	foreach ( $archives as $post ) {
		$html_tag .= '<li><a href="'.get_permalink($post->ID).'">'.'<span class="c-sidebar_post_title">'.get_the_title($post->ID).'</span>'.'</a></li>';
	}
	if ( $html_tag ) {
		return '<div class="l-sidebar_post_list is-new">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}

//最新の記事リスト - 施工事例・お知らせ
function side_works_list( $list_title = NULL, $post_type = NULL, $num = 5) {
	$title_tag = ( $list_title ) ? '<span class="headline">'.$list_title.'</span>' : '';
	$html_tag  = '';
	$args      = array(
		'post_type'      => $post_type,
		'order'          => 'DESC',
		'posts_per_page' => $num,
	);
	$archives = get_posts($args);
	foreach ( $archives as $post ) {
		$thumbnail = create_thumbnail($post,'','side-thumbnail');
		$html_tag .= '<div class="side_works_box"><li><a href="'.get_permalink($post->ID).'">'.$date.'<span class="c-sidebar_post_title">'.get_the_title($post->ID).'</span>'.'</a></li></div>';
	}
	if ( $html_tag ) {
		return '<div class="side_works_list">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}
//カテゴリリスト
function side_categories( $list_title = NULL, $taxonomy_name = NULL, $num_view = false ) {
	$title_tag     = ( $list_title ) ? '<p class="title">'.$list_title.'</p>' : '';
	$html_tag      = '';
	$args          = array(
		'orderby' => 'menu_order',
		'order'   => 'ASC',
	);
	$terms         = get_terms($taxonomy_name, $args);
	foreach ( $terms as $term ) {
		$html_tag .= '<li><a href="'.get_term_link($term).'"><span>'.$term->name.'（'.$term->count.'）'.'</span></a></li>';
	}
	if ( $html_tag ) {
		return '<div class="l-sidebar_post_list is-category">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}
//カテゴリリスト施工事例
function side_works_categories( $list_title = NULL, $taxonomy_name = NULL, $num_view = false ) {
	$title_tag     = ( $list_title ) ? '<span class="headline">'.$list_title.'</span>' : '';
	$html_tag      = '';
	$args          = array(
		'orderby' => 'menu_order',
		'order'   => 'ASC',
		'hide_empty' => 0
	);
	$terms         = get_terms($taxonomy_name, $args);
	foreach ( $terms as $term ) {
		$html_tag .= '<li><a href="'.get_term_link($term).'">'.$term->name.'</a></li>';
	}
	if ( $html_tag ) {
		return '<div class="cat-list">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}

//オススメの記事リスト（フッター） - 施工事例・お知らせ
function footer_works_recomend( $list_title = 'おすすめ事例', $post_type = works, $num = 3) {
	$title_tag = ( $list_title ) ? '<span class="headline">'.$list_title.'</span>' : '';
	$html_tag  = '';
	$args      = array(
		'post_type'      => $post_type,
		'order'          => 'DESC',
		'posts_per_page' => $num,
	);
	$archives = get_posts($args);
	foreach ( $archives as $post ) {
		$thumbnail = create_thumbnail($post,'','side-thumbnail');
		$html_tag .= '<div class="side_works_box"><li><a href="'.get_permalink($post->ID).'">'.$date.'<span class="c-sidebar_post_title">'.get_the_title($post->ID).'</span>'.'</a></li></div>';
	}
	if ( $html_tag ) {
		return '<div class="side_works_list">'.$title_tag.'<ul>'.$html_tag.'</ul></div>';
	}
}



?>
