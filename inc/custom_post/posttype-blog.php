<?php
//耐震ニュース・コラムに関する関数です
/*====================================
ポストタイプ情報を格納
==================================== */
function posttype_info_blog() {
	$name  = 'blog';
	$label = '耐震ニュース＆コラム';
	$posttype_info = array(
		'name'          => $name,
		'label'         => $label,
		'category_name' => $name.'_category',
		'description'   => '耐震に関するニュースとコラムを掲載しております',
	);
	return $posttype_info;
}
/* ===================================
カスタム投稿（タクソノミー対応）を生成
=================================== */
function create_posttype_blog(){
	$posttype_info = posttype_info_blog();
	$theme_url     = get_template_directory_uri();
	register_post_type(
		$posttype_info['name'],
		array(
			'label'        => $posttype_info['label'],
			'hierarchical' => false,
			'public'       => true,
			'has_archive'  => true,
			'supports'     => array( 'title', 'editor', 'revisions', 'thumbnail' ),
		)
	);
	register_taxonomy(
	$posttype_info['category_name'], //分類名
	array($posttype_info['name']), // 紐づけるカスタム投稿タイプ（複数可）
	array(
		'hierarchical'      => true,
		'label'             => $posttype_info['label'].'カテゴリー',
		'singular_label'    => $posttype_info['label'].'カテゴリー',
		'public'            => true,
		'show_ui'           => true,
		'show_admin_blog'   => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => $posttype_info['name']
		),
	)
	);
}
add_action( 'init', 'create_posttype_blog' );

/* ===================================
アーカイブ　表示条件を変更
=================================== */
function blog_archive_args_change($query) {
	$posttype_info = posttype_info_blog();
	if ( is_admin() || ! $query->is_main_query() )
		return;
	if ( $query->is_post_type_archive( $posttype_info['name'] ) ) {
		$query->set( 'posts_per_page', '15' ); //表示件数
	}
}
add_action( 'pre_get_posts', 'blog_archive_args_change' );

/* ===================================
アーカイブテンプレート用のtitle、description設定
=================================== */
add_filter( 'aioseop_description', 'custom_aioseop_description_blog' );
function custom_aioseop_description_blog( $description ) {
	$posttype_info = posttype_info_blog();
	if ( is_post_type_archive( $posttype_info['name'] ) ) {
		$description = $posttype_info['description'];
	}
	return $description;
}

/*====================================
管理画面のカスタマイズ
==================================== */
//記事一覧に「表示場所」列を追加する処理
function add_custom_column_blog($defaults) {
	$defaults[blog_category] = 'カテゴリ';
	return $defaults;
}
add_filter('manage_blog_posts_columns', 'add_custom_column_blog');

//記事一覧に追加した表示列にカスタムタクソノミーを表示する処理
function add_custom_column_id_blog($column_name, $id) {
	if ( $column_name == 'blog_category' ) {
		$terms = get_the_terms($id, 'blog_category');
		if(isset($terms) && !is_wp_error($terms)){
			$cnt = 0;
			foreach($terms as $term){
				if($cnt < 1){
					echo $term->name;
				} else {
					echo ' , '.$term->name;
				}
				$cnt += 1;
			}
		}
	}
}
add_action('manage_blog_posts_custom_column', 'add_custom_column_id_blog', 10, 2);

/*====================================
サイドバーに最新5件を表示（サムネイルあり）
==================================== */
function get_side_blog_list($num){
global $post;

	$blog_list = '';

	$paged = get_query_var('paged');
		query_posts( array(
			'post_type' => 'blog',
			'taxonomy' => 'blog_category',
			// 'term' => 'blog'
			'posts_per_page' => $num,
		));

		// $url = array('start' => '', 'end' => '</a>' , 'content' => '');
		// $url['start'] = '<a href="'.get_permalink().'">';
		// $news_list .= '<div class="p-side_blog">' ."\n";
		// $news_list .= '<div class="p-side_news_title"><a href="' .home_url().'/news/news/" class="maintitle">お知らせ</a><a href="' .home_url().'/news/news/" class="link">一覧</a></div>' ."\n";
		$blog_list .= '<div class="l-sidebar_post_list is-new_thumb">' ."\n";
		$blog_list .= '<a href="' .home_url().'/blog/" class="title">お知らせ・ブログ</p>' ."\n";
		$blog_list .= '<ul>' ."\n";

		if(have_posts()):while(have_posts()):the_post();
		$blog_list .= '<li>';

		//サムネイル
		$thumb_path = create_thumbnail_path($post, get_template_directory_uri().'/assets/images/p-blog_thumb.jpg');
		$blog_list_thumb = '<a href="'.get_permalink().'" class="thumb" style="background-image:url('.$thumb_path.')"></a>' ."\n";

		// 日付
		// $blog_list_date = '<p class="date">' .get_the_time('Y.m.d') .'</p>';

		//カテゴリ
		$terms = get_the_terms( $post->ID, 'blog_category' );
			if ( !empty($terms) ) {
				if ( !is_wp_error( $terms ) ) {
					foreach( $terms as $term ) {
				}
			}
		}
		$blog_list_category = '<p  class="p-blog_category"><span>'. $term->name .' </span></p>';

		// タイトル
		$blog_list_title = '<a href="'.get_permalink().'" class="title">' .$post->post_title .'</a>';

		// 表示内容
		$blog_list .= $blog_list_thumb;
		$blog_list .= '<div class="info">' ."\n";
		$blog_list .= $blog_list_category;
		$blog_list .= $blog_list_title;
		$blog_list .= '</div>' ."\n";
		$blog_list .= '</li>' ."\n";
	endwhile;

	wp_reset_query();
	$blog_list .= '</ul>' ."\n";
	$blog_list .= '</div>' ."\n";

	endif;
	return $blog_list;
}
add_shortcode('blog_list', 'get_side_blog_list');

/*====================================
トップにニュース一覧を表示
==================================== */
function output_home_bloglist($atts) {
	extract( shortcode_atts( array(
		'num'         => 4, //表示件数
		'date_format' => 'Y.m.d',
	), $atts));
	$htmltag       = '';
	$posttype_info = posttype_info_blog();
	$args = array(
		'posts_per_page' => $num,
		'post_type'      => $posttype_info['name'],
	);
	$bloglist = get_posts($args);
	if ( $bloglist ) {
		foreach ( $bloglist as $list ) : setup_postdata( $list );
			$args = array(
				'orderby' => 'term_order',
				'fields'  => 'all'
			);
			$terms      = wp_get_object_terms( $list->ID, 'blog_category', $args );
			$post_term  = '';
			if ( !empty($terms) && !is_wp_error($terms) ) {
				foreach($terms as $term){
					$blog_category_slug = $term->slug;
					$blog_category_name = $term->name;
				}
			}
			$htmltag   .= '<li>';
			$thumb_path = create_thumbnail_path($list, get_template_directory_uri().'/assets/images/p-blog_thumb_home.jpg');
			$htmltag   .= '<a href="'.get_permalink($list).'" class="p-blog_thumb" style="background-image: url('.$thumb_path.');"></a>';
			$htmltag   .= '<div class="p-blog_info">';
			$htmltag   .= '<div class="p-blog_note">';
			$htmltag   .= '<p class="date">'.get_the_time($date_format,$list).'</p>';
			$htmltag   .= '<p class="category"><span class="cate_'.$blog_category_slug.'">'.$blog_category_name.'</span></p>';
			$htmltag   .= '</div>';
			$htmltag   .= '<a href="'.get_permalink($list).'" class="title">'.mb_strimwidth(get_the_title($list), 0, 80, '…').'</a>';
			$htmltag   .= '</div>';
			$htmltag   .= '</li>';
		endforeach;
	}
	wp_reset_postdata();
	if ( $htmltag ) {
		return '<ul class="p-blog_home_list">'.$htmltag.'</ul>';
	} else {
		return '<p class="p-blog_home_list_noData">※現在、耐震ニュース＆コラムはありません。</p>';
	}
}
add_shortcode('output_home_bloglist', 'output_home_bloglist');

//編集画面のカテゴリをチェックボックス→ラジオボタンに変更
function my_print_footer_scripts_blog() {
	echo '<script type="text/javascript">
	//<![CDATA[
	jQuery(document).ready(function($){
	$(".categorychecklist input[type=checkbox]").each(function(){
	$check = $(this);
	var checked = $check.attr("checked") ? \' checked="checked"\' : \'\';
	$(\'<input type="radio" id="\' + $check.attr("id")
	+ \'" name="\' + $check.attr("name") + \'"\'
  	+ checked
	+ \' value="\' + $check.val()
	+ \'"/>\'
      ).insertBefore($check);
      $check.remove();
	});
	});
  	//]]>
	</script>';
}
add_action('admin_print_footer_scripts', 'my_print_footer_scripts_blog', 21);
?>