<?php
//トップページに関する関数です
//totta.biz
/*====================================
ポストタイプ情報を格納
==================================== */
function posttype_info_home() {
	$name  = 'home';
	$label = 'トップページ';
	$posttype_info = array(
		'name'          => $name,
		'label'         => $label,
		'category_name' => $name.'_category',
	);
	return $posttype_info;
}

/* ===================================
カスタム投稿（タクソノミー対応）を生成
=================================== */
function create_posttype_home(){
	$posttype_info = posttype_info_home();
	$theme_url = get_template_directory_uri();
	register_post_type(
		$posttype_info['name'],
		array(
			'label'        => $posttype_info['label'],
			'hierarchical' => true,
			'public'       => true,
			'query_var'    => true,
			'has_archive'  => false,
			'supports'     => array( 'title', 'editor', 'page-attributes', 'revisions' ),
		)
	);
	register_taxonomy(
	$posttype_info['category_name'], //分類名
	array($posttype_info['name']), // 紐づけるカスタム投稿タイプ（複数可）
	array(
		'hierarchical'   => true,
		'label'          => '種別',
		'singular_label' => '種別',
		'public'         => false,
		'show_ui'        => true,
		'show_admin_column' => true,
		'query_var'      => true,
	)
	);
}
add_action( 'init', 'create_posttype_home' );

/*====================================
呼び出されたカテゴリ（場所）の各パーツを出力するショートコード
==================================== */
//トップページパーツの指定カテゴリ記事を取得
function get_home_parts($num = -1 ,$term = ''){
	$posttype_info = posttype_info_home();
	$args = array(
		'post_type'      => $posttype_info['name'],
		'posts_per_page' => $num, // 取得する件数
		'orderby'        => 'menu_order',
		'order'          => 'ASC',
		'tax_query'      => array(
			array(
				'taxonomy' => $posttype_info['category_name'],
				'field' => 'slug',
				'terms' => $term,
			)
		),
	);
	$parts_query = new WP_Query( $args );
	return $parts_query;
}

//指定されたカテゴリ（場所）のパーツを出力（ノーマルパーツ）
function output_home_parts_normal($term){
	$num = -1;
	$parts_roop = '';
	$get_parts = get_home_parts($num,$term);
	if ( $get_parts->have_posts() ) :
		while ( $get_parts->have_posts() ) : $get_parts->the_post();
			$post        = get_post();
			$slug        = $post->post_name;
			$parts_roop .= '<div class="p-'.$slug.'">';
			$parts_roop .= do_shortcode(get_the_content());
			$editlink = get_edit_post_link();
			if ( $editlink ) { $parts_roop .= '<p class="p-editlink"><a href="'.$editlink.'">このパーツを編集</a></p>'; }
			$parts_roop .= '</div>';
		endwhile;
	endif;
	wp_reset_postdata();
	return $parts_roop;
}

//メイン画像出力
function output_home_parts_slide() {
	$num         = -1;
	$term        = 'mainimg';
	$parts_query = get_home_parts( $num, $term );
	if ( $parts_query->have_posts() ) {
		$htmltag = '<div class="swiper-container p-home_contents_item__inner p-home_mainimg__inner"><ul class="swiper-wrapper p-home_mainimg__list">'."\n";
		while ( $parts_query->have_posts() ) {
			$parts_query->the_post();
			$post   = get_post();
			$images = get_field('mainimg');
			foreach ($images as $image) {
				if ( $image['link_flg'] != 'none' && $image['link_url'] ) {
					$wrap_start = '<a href="'.$image['link_url'].'">';
					$wrap_end   = '</a>';
				} elseif ( $image['link_flg'] != 'none' && $image['link_url_other'] ) {
					$wrap_start = '<a href="'.$image['link_url_other'].'" target="_blank">';
					$wrap_end   = '</a>';
				} else {
					$wrap_start = '';
					$wrap_end   = '';
				}
				$htmltag .= '<li class="swiper-slide">'.$wrap_start.'<img src="'.$image['img'].'" />'.$wrap_end.'</li>';
			}
		} //while
		$htmltag .= "</ul>\n";
		$htmltag .= '<div class="swiper-button-prev swiper-button-white p-home_mainimg__prev"></div><div class="swiper-button-next swiper-button-white p-home_mainimg__next"></div>';
		$htmltag .= ( get_edit_post_link() ) ? '<p class="p-editlink"><a href="'.get_edit_post_link().'">画像を編集</a></p>'."\n" : '';
		$htmltag .= "</div>\n";
	} else {
		$htmltag = '';
	}
	wp_reset_query();
	return $htmltag;
}

/*====================================
種別を複数選択できないようにする
参考：http://kachibito.net/wordpress/limit_checkbox.html
==================================== */
function limit_checkbox_amount() {
echo '<script type="text/javascript">
jQuery("ul#home_categorychecklist").before("<p>※どれかひとつにチェックしてください</p>");
	var count = jQuery("ul#home_categorychecklist li input[type=checkbox]:checked").length;
	var not = jQuery("ul#home_categorychecklist li input[type=checkbox]").not(":checked");
	if(count >= 1) { not.attr("disabled",true);}else{ not.attr("disabled",false);}

jQuery("ul#home_categorychecklist li input[type=checkbox]").click(function(){
	var count = jQuery("ul#home_categorychecklist li input[type=checkbox]:checked").length;
	var not = jQuery("ul#home_categorychecklist li input[type=checkbox]").not(":checked");
	if(count >= 1) { not.attr("disabled",true);}else{ not.attr("disabled",false);}
});
  </script>';
}
add_action('admin_footer', 'limit_checkbox_amount');
?>