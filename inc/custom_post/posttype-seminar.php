<?php
//セミナーページに関する関数です

/*====================================
ポストタイプ情報を格納
==================================== */
function posttype_info_seminar() {
	$name  = 'seminar';
	$label = 'イベント・セミナー';
	$posttype_info = array(
		'name'                => $name,//カスタム投稿名
		'label'               => $label,
		'category_name'       => $name.'_category', //カスタムタクソノミー名
		'description'         => 'Wallstat主催で開催予定のイベント・セミナー情報を掲載しております',
		'open_days_filterkey' => 'field_5ac2d6a5c57d1', //開催日フィールドのフィルターキーを設定
	);
	return $posttype_info;
}

/* ===================================
カスタム投稿（タクソノミー対応）を生成
=================================== */
function create_posttype_seminar(){
	$posttype_info = posttype_info_seminar();
	$theme_url     = get_template_directory_uri();
	register_post_type(
		$posttype_info['name'],
		array(
			'label'        => $posttype_info['label'],
			'hierarchical' => false,
			'public'       => true,
			'has_archive'  => true,
			'supports'     => array( 'title',  'editor', 'revisions', 'thumbnail' ),
			'menu_icon'    => 'dashicons-businessman',
		)
	);
	register_taxonomy(
	$posttype_info['category_name'], //分類名
	array($posttype_info['name']), // 紐づけるカスタム投稿タイプ（複数可）
	array(
		'hierarchical'      => true,
		'label'             => 'セミナー種別',
		'singular_label'    => 'セミナー種別',
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array(
			'slug' => $posttype_info['name']
		),
	)
	);
	//予約ステータス
	register_taxonomy(
	'reserve_status', //分類名
	array($posttype_info['name']), // 紐づけるカスタム投稿タイプ（複数可）
	array(
		'hierarchical'      => true,
		'label'             => '予約ステータス',
		'singular_label'    => '予約ステータス',
		'public'            => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	)
	);
}
add_action( 'init', 'create_posttype_seminar' );

/* ===================================
アーカイブ　表示条件を変更
=================================== */
function seminar_archive_args_change($query) {
	$posttype_info = posttype_info_seminar();
	if ( is_admin() || ! $query->is_main_query() )
		return;
	if ( $query->is_post_type_archive( $posttype_info['name'] ) ) {
		$query->set( 'posts_per_page', '1' ); //表示件数
	}
}
add_action( 'pre_get_posts', 'seminar_archive_args_change' );

/* ===================================
アーカイブテンプレート用のtitle、description設定
=================================== */
add_filter( 'aioseop_description', 'custom_aioseop_description_seminar' );
function custom_aioseop_description_seminar( $description ) {
	$posttype_info = posttype_info_seminar();
	if ( is_post_type_archive( $posttype_info['name'] ) ) {
		$description = $posttype_info['description'];
	}
	return $description;
}

/* ===================================
管理画面 - 記事一覧に募集ステータス、開催日を表示
=================================== */
//記事一覧に表示列を追加
function add_seminar_custom_column($defaults) {
	$defaults['openday'] = '開催日';
	return $defaults;
}
add_filter('manage_seminar_posts_columns', 'add_seminar_custom_column');

//記事一覧に追加した表示列に各記事の募集ステータス・開催日を表示
function add_seminar_custom_column_id($column_name, $id) {
	$opendays = seminar_openday_string($id);
	if($column_name == 'openday') {
		echo $opendays;
	}
}
add_action('manage_seminar_posts_custom_column', 'add_seminar_custom_column_id', 10, 2);


/* ===================================
編集画面で必要な関数
=================================== */
//一番早い開催日と遅い開催日を保存（これから開催と過去開催判断に使用）
function save_far_opendays($data , $postarr){
	$posttype_info = posttype_info_seminar();
	if ( $data['post_type'] == $posttype_info['name'] ) {
		$acf_filter_key = $posttype_info['open_days_filterkey']; //該当フィールドのフィールドキー
		$post_id        = $postarr['ID'];
		if ( isset($_POST['fields'][$acf_filter_key]) ) {
			$input_opendays = $_POST['fields'][$acf_filter_key];
			unset( $input_opendays['acfcloneindex'] );
			$input_opendays = format_seminar_openday($input_opendays);
			if ( count( $input_opendays ) < 2 ) {
				$min_day        = $input_opendays[0];
				$max_day        = $input_opendays[0];
			} else {
				$min_day        = array_shift($input_opendays);
				$max_day        = array_pop($input_opendays);
			}
			if ( !add_post_meta($post_id, 'open_day_min', $min_day, true ) ) {
				update_post_meta($post_id, 'open_day_min', $min_day);
			}
			if ( !add_post_meta($post_id, 'open_day_max', $max_day, true ) ) {
				update_post_meta($post_id, 'open_day_max', $max_day);
			}
		}
	}
	return $data;
}
add_filter( 'wp_insert_post_data', 'save_far_opendays', '99', 2 );

//編集画面で予約ステータスをラジオ形式に変更
function my_print_footer_scripts() {
echo <<<EOT
<script type='text/javascript'>
jQuery(document).ready(function($){
	var checkLists = $('#reserve_statuschecklist').find('li');
	var cnt = 0;
	checkLists.each(function(){
		var check = $(this).find('input');
		var input = $('<input>');
		input.attr({
			type: 'radio',
			id: check.attr('id'),
			name: check.attr('name'),
			value: check.val()
		});
		if($(this).hasClass('popular-category') && cnt === 0){
		   input.prop('checked', true);
		   cnt++
		}
		input.insertBefore(check);
		check.remove();
	});
});
</script>
EOT;
}
add_action('admin_print_footer_scripts', 'my_print_footer_scripts', 21);


/* ===================================
テンプレート出力に必要な関数
=================================== */
//// 全体 ////
//$_GETを使用するための変数宣言
function add_seminar_query_vars( $public_query_vars ) {
	$public_query_vars[] = 'past';
	$public_query_vars[] = 'entry_id';
	return $public_query_vars;
}
add_filter( 'query_vars', 'add_seminar_query_vars' );

//本日以降に開催されるイベント・セミナーを取得（get_post結果をreturn）
function get_seminar_later_list($post_type, $num, $taxonomy = 'seminar_category', $term = '') {
	$today       = date('Ymd');
	if ( $term ) {
		$tax_query = array( array(
			'taxonomy' => $taxonomy,
			'field'    => 'slug',
			'terms'    => $term
		));
	} else {
		$tax_query = '';
	}
	$args        = array(
		'numberposts'     => $num,
		'post_type'       => $post_type,
		'post_status'     => 'publish',
		'orderby'         => 'meta_value',
		'order'           => 'ASC',
		'meta_key'        => 'open_day_min',
		'meta_query'      => array(
			array(
				'key'     => 'open_day_max',
				'value'   => $today,
				'type'    => 'DATE',
				'compare' => '>='
			)
		),
		'tax_query'       => $tax_query,
	);
	return get_posts($args);
}

//開催日に入る文言を生成
function seminar_openday_string($post, $format = array('year' => '年', 'month' => '月', 'day' => '日', 'youbi' => 'visible')) {
	$hikaku         = strtotime('19000101'); //ダミー値を設定
	$openday_string = array();
	$open_days      = format_seminar_openday( get_field('open_days',$post->ID) );
	foreach ($open_days as $day) {
		$day_date = strtotime($day);
		if ( date('Y',$hikaku) != date('Y',$day_date) ) { //年違い
			$hikaku = $day_date;
			$join   = ( count($openday_string) >= 1 ) ? '、' : '';
			$youbi  = ( $format['youbi'] == 'visible' ) ? '('.output_youbi($day_date).')' : '';
			array_push($openday_string, $join .date( 'Y'.$format['year'].'n'.$format['month'].'j'.$format['day'].$youbi, $day_date ) );
		} elseif ( date('m',$hikaku) != date('m',$day_date) ) { //月違い
			$join   = '、';
			$hikaku = $day_date;
			$youbi  = ( $format['youbi'] == 'visible' ) ? '('.output_youbi($day_date).')' : '';
			array_push($openday_string, $join.date( 'n'.$format['month'].'j'.$format['day'].$youbi, $day_date ) );
		} elseif ( date('d',$hikaku) != date('d',$day_date) ) { //日違い
			$join   = '・';
			$hikaku = $day_date;
			$youbi  = ( $format['youbi'] == 'visible' ) ? '('.output_youbi($day_date).')' : '';
			array_push($openday_string, $join.date( 'j'.$format['day'].$youbi, $day_date ) );
		}
	}
	$openday_string = join('',$openday_string);
	return $openday_string;
}

//募集の判断
function seminar_entry_info($post) {
	$entry_info     = array();
	$open_days      = format_seminar_openday( get_field('open_days',$post->ID) ); //整形された開催日リスト
	$reserve_status = get_seminar_reserve_status($post); //予約ステータスを取得
	if ( !has_after_target_day($open_days, date('Ymd',strtotime("-1 day"))) ) { //イベントが昨日以前
		$entry_info['head_message'] = 'このセミナーは終了しました';
		$entry_info['info_message'] = 'このセミナーは終了しました';
		$entry_info['entry']        = false;
	} elseif ( !has_after_target_day($open_days, date('Ymd',strtotime("+1 day"))) ) { //イベント当日
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '申し訳ございません。セミナー当日の申し込みは承っておりません。';
		$entry_info['entry']        = false;
	} elseif ( $reserve_status == 'no_reserve' && has_after_target_day($open_days, date('Ymd')) ) {
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '申し込みの必要はございません。当日、直接会場にお越しください。';
		$entry_info['entry']        = false;
	} elseif ( $reserve_status == 'reserve_before' && has_after_target_day($open_days, date('Ymd',strtotime("+1 day"))) ) {
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '参加申込受付前です。もうしばらくお待ちください。';
		$entry_info['entry']        = false;
	} elseif ( $reserve_status == 'reserve_ng' && has_after_target_day($open_days, date('Ymd',strtotime("+1 day"))) ) {
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '申し訳ございません。セミナーの申し込みは締め切りました。';
		$entry_info['entry']        = false;
	} elseif ( $reserve_status == 'reserve_ok' && has_after_target_day($open_days, date('Ymd',strtotime("+1 day"))) ) {
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '<a href="'.home_url().'/seminar_entry/?post_id='.$post->ID.'" class="p-seminar_entry_btn">イベント・セミナーのお申込みはこちら</a>';
		$entry_info['entry']        = true;
	} else {
		$entry_info['head_message'] = '';
		$entry_info['info_message'] = '';
		$entry_info['entry']        = false;
	}
	return $entry_info;
}

//開催日を各関数で使用できるように成形
function format_seminar_openday($days) {
	$format_days = array();
	if ( is_array($days) ) {
		$format_days = get_flat_array($days);
		$format_days = array_unique($format_days); //重複値を削除
		natsort ($format_days); //ソート
	}
	return (array)$format_days;
}

//予約ステータスを取得
function get_seminar_reserve_status($post) {
	$terms = get_the_terms($post->ID, 'reserve_status');
	foreach((array)$terms as $term){
		$reserve_status = $term->slug;
	}
	return $reserve_status;
}

//開催日の中に指定された日付以降があるか判断
function has_after_target_day( $days, $target_day ) {
	$has_after = false;
	foreach ($days as $day) {
		if ( strtotime($day) >= strtotime($target_day) ) {
			$has_after = true;
			break;
		}
	}
	return $has_after;
}


//レポートがあるか判断
function has_seminar_report($post){
	$report = get_field('report',$post->ID);
	$report = strip_tags($report); //HTMLタグの除去
	$report = trim("$report"); //スペース、改行、タブの削除
	if ( $report ) {
		return true;
	} else {
		return false;
	}
}

//多次元配列を単純配列へ（キーがリセットするため要注意）参考：http://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q13138449293
function get_flat_array($array){
	$it   = new RecursiveArrayIterator($array);
	$it   = new RecursiveIteratorIterator($it);
	$flat = iterator_to_array($it, false);
	return $flat;
}

/* ===================================
アーカイブの表示条件
=================================== */
//表示条件を変更
function seminar_args_change($query) {
	$posttype_info = posttype_info_seminar();
	$past_num      = ( isset($_GET['past']) && $_GET['past'] == 'all' ) ? 15 : 15; //パラメータ値がある場合は100件づつ
	$meta_query    = array(
		array(
			'key'     => 'open_day_max',
			'value'   => date('ymd'),
			'type'    => 'DATE',
			'compare' => '<'
		)
	);
	if ( is_admin() || ! $query->is_main_query() )
		return;
	if ( $query->is_post_type_archive( $posttype_info['name'] ) || $query->is_tax($posttype_info['category_name']) ) {
		$query->set( 'post_type', $posttype_info['name']);
		$query->set( 'posts_per_page', $past_num );
		$query->set( 'orderby', 'meta_value_num' );
		$query->set( 'order', 'DESC' );
		$query->set( 'meta_key', 'open_day_max' );
		$query->set( 'meta_query', $meta_query );
	}
}
add_action( 'pre_get_posts', 'seminar_args_change' );


/* ===================================
イベント・セミナーページ用
=================================== */
//指定タームのこれからのイベント・セミナーを出力
function output_seminar_laterlist($atts){
	extract(shortcode_atts(array(
		'num'  => -1,
		'term' => '',
	), $atts));
	$posttype_info = posttype_info_seminar();
	$posts         = get_seminar_later_list($posttype_info['name'],$num,$posttype_info['category_name'],$term);
	$date_format   = array('year' => '年', 'month' => '月', 'day' => '日', 'youbi' => 'visible');
	foreach ( $posts as $post ) {
		$date      = '<span class="seminar_laters_date">'.seminar_openday_string($post, $date_format).'</span>';
		$html_tag .= '<li class="clearfix">'.$date.' '.get_field('date_time',$post->ID).'</li>';
	}
	if ( $html_tag ) {
		return '<div class="seminar_laters"><ul class="list_style01">'.$html_tag.'</ul></div>';
	} else {
		return '<div class="seminar_laters" style="color:#fff;">これから開催されるイベント・セミナーはありません</div>';
	}
}
add_shortcode('output_seminar_laterlist', 'output_seminar_laterlist');

/* ===================================
申し込みフォーム用
=================================== */
//選択したイベントをフォーム入力画面に表示
function output_seminar_select_title(){
	$entry_id  = ( isset( $_GET['post_id'] ) ) ? $_GET['post_id'] : '';
	$entry_obj = get_post($entry_id);
	$days      = seminar_openday_string($entry_obj).'<span class="u-ml_m">'.get_field('date_time',$entry_id).'</span>';
	$hidden    = '<input type="hidden" value="'.$entry_id.'" name="seminar_id">';
	return $days.'<br>'.get_the_title($entry_id).$hidden;
}
wpcf7_add_shortcode('output_seminar_select_title', 'output_seminar_select_title');

//選択したイベント情報をメールに表示
function output_seminar_entry_mail($output, $name){
	$name = preg_replace( '/^wpcf7\./', '_', $name );
	$submission = WPCF7_Submission::get_instance(); //送信データを取得
	if ( ! $submission ) {
		return $output;
	}
	$data = $submission -> get_posted_data();
	if('seminar_title' == $name) {
		$seminar_id  = $data['seminar_id'];
		$entry_obj   = get_post($seminar_id);
		$output_text = seminar_openday_string( $entry_obj )."\n".get_the_title($seminar_id); //タイトル・開催日
		return $output_text;
	}
	return $output;
}
add_filter('wpcf7_special_mail_tags', 'output_seminar_entry_mail', 10 ,2);

/*====================================
サイドバー用
==================================== */
function get_side_seminar_list($num) {
	$htmltag       = '<div class="p-side_seminar">'."\n";
	$htmltag      .= '<p class="p-side_seminar__title"><a href="'.home_url('/seminar/').'"><span class="main">撮影入門セミナー</span><span class="archive">一覧</span></a></p>'."\n"; //タイトル
	$date_format   = array('year' => '年', 'month' => '月', 'day' => '日', 'youbi' => 'visible');
	$posttype_info = posttype_info_seminar();
	$posts         = get_seminar_later_list($posttype_info['name'], $num);
	if ( $posts ) {
		$htmltag .= '<ul class="p-side_seminar__list">'."\n";
		foreach ( $posts as $list ) : setup_postdata( $list );
			$htmltag   .= '<li><a href="'.get_permalink($list).'">';
			$thumb_path = create_thumbnail_path($list, get_template_directory_uri().'/images/thumbnail_dummy.png');
			$htmltag   .= '<div class="p-side_seminar__thumb" style="background-image:url('.$thumb_path.')"></div>'."\n";
			$htmltag   .= '<div class="p-side_seminar__info">'."\n";
			$entry_info = seminar_entry_info($list);
			if ( $entry_info['entry'] == true ) {
				$htmltag .= '<p class="p-side_seminar__status is-ok">受付中</p>'."\n";
			} elseif ( $entry_info['info_message'] == '参加申込受付前です。もうしばらくお待ちください。' ) {
				$htmltag .= '<p class="p-side_seminar__status is-before">受付準備中</p>'."\n";
			} elseif ( $entry_info['info_message'] == '申し込みの必要はございません。当日、直接会場にお越しください。' ) {
				$htmltag .= '<p class="p-side_seminar__status is-ok">受付中</p>'."\n";
			} else {
				$htmltag .= '<p class="p-side_seminar__status is-end">受付終了</p>'."\n";
			}
			$htmltag   .= '<p class="p-side_seminar__date">'.seminar_openday_string($list, $date_format).' 開催'.'</p>'."\n";
			$htmltag   .= '</div>'."\n";
			$htmltag   .= '<p class="p-side_seminar__post_title">'.get_the_title($list).'</p>';
			$htmltag   .= '</a></li>';
		endforeach;
		$htmltag .= '</ul>'."\n";
	} else {
		$htmltag .= '<p class="p-side_seminar__nodata">これから開催されるセミナーはありません</p>';
	}
	wp_reset_postdata();
	$htmltag      .= '</div>'."\n";
	return $htmltag;
}

/*====================================
トップページ用
==================================== */
function view_side_seminarlist($atts) {
	extract( shortcode_atts( array(
		'num'         => 1, //表示件数
	), $atts));
	$htmltag       = '';
	$date_format   = array('year' => '年', 'month' => '月', 'day' => '日', 'youbi' => 'visible');
	$posttype_info = posttype_info_seminar();
	$posts         = get_seminar_later_list($posttype_info['name'], $num);
	if ( $posts ) {
		$htmltag .= '<div class="p-home_seminar_box">'."\n";
		foreach ( $posts as $list ) : setup_postdata( $list );
			$args  = array(
				'orderby' => 'term_order',
				'fields'  => 'all'
			);

			$htmltag   .= '<p class="p-home_seminar_box_date">'.seminar_openday_string($list, $date_format).'  【時間】'.get_field('date_time', $list->ID).'</p>'."\n";
			$htmltag   .= '<a class="p-home_seminar_box_title" href="'.get_permalink($list).'">'.get_the_title($list).'</a>';
			$htmltag   .= '<div class="p-home_seminar_box_content">'.get_field('content', $list->ID).'</div>';
			$htmltag   .= '<a class="p-home_seminar_box_link" href="'.get_permalink($list).'">>>詳しくはこちら</a>';

		endforeach;
		$htmltag .= '</div>'."\n";
	} else {
		$htmltag .= '<p class="p-home_seminar__nodata">これから開催されるセミナーはありません</p>';
	}
	wp_reset_postdata();
	return $htmltag;
}
add_shortcode('view_side_seminarlist', 'view_side_seminarlist');



// function view_side_seminarlist($atts) {
// 	extract( shortcode_atts( array(
// 		'num'         => 3, //表示件数
// 	), $atts));
// 	$htmltag       = '';
// 	$date_format   = array('year' => '年', 'month' => '月', 'day' => '日', 'youbi' => 'false'); //曜日表示不要の場合
// 	$posttype_info = posttype_info_seminar();
// 	$posts         = get_seminar_later_list($posttype_info['name'], $num);
// 	if ( $posts ) {
// 		$htmltag .= '<ul class="p-home_seminar__list">'."\n";
// 		foreach ( $posts as $list ) : setup_postdata( $list );
// 			$args  = array(
// 				'orderby' => 'term_order',
// 				'fields'  => 'all'
// 			);
// 			$terms = wp_get_object_terms( $list->ID, 'seminar_category', $args );
// 			if ( !empty($terms) && !is_wp_error($terms) ) {
// 				$post_term = '<div class="p-home_seminar__list_term">';
// 				foreach($terms as $term){
// 					$post_term .= '<span class="'.$term->slug.'">'.$term->name.'</span>';
// 				}
// 				$post_term .= '</div>';
// 			}
// 			$htmltag   .= '<li><a href="'.get_permalink($list).'">';
// 			$thumb_path = create_thumbnail_path($list, get_template_directory_uri().'/images/thumbnail_dummy.png');
// 			$htmltag   .= '<div class="p-home_seminar__list_thumb" style="background-image:url('.$thumb_path.')"></div>'."\n";
// 			$htmltag   .= '<div class="p-home_seminar__list_text">'."\n";
// 			$htmltag   .= '<div class="p-home_seminar__list_text_head">'."\n";
// 			$htmltag   .= $post_term."\n";
// 			$htmltag   .= '<p class="p-home_seminar__list_date">'.seminar_openday_string($list, $date_format).'</p>'."\n";
// 			$htmltag   .= '</div>'."\n";
// 			$htmltag   .= '<p class="p-home_seminar__list_post_title">'.get_the_title($list).'</p>';
// 			$htmltag   .= '</div>';
// 			$htmltag   .= '</a></li>';
// 		endforeach;
// 		$htmltag .= '</ul>'."\n";
// 	} else {
// 		$htmltag .= '<p class="p-home_seminar__nodata">これから開催されるセミナーはありません</p>';
// 	}
// 	wp_reset_postdata();
// 	return $htmltag;
// }
// add_shortcode('view_side_seminarlist', 'view_side_seminarlist');
 ?>