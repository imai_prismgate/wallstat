<?php
/*
 * @package WordPress
 * @subpackage wallstat
*/

header('HTTP/1.0 404 Not Found');
include(TEMPLATEPATH.'/404.php');
exit;

?>

<?php get_header(); ?>
	<h1><?php the_title_attribute(); ?></h1>
	<?php the_content(); ?>
<?php get_footer(); ?>

<?php // CODE IS POETRY ?>