<?php
/*
 * @package WordPress
 * @subpackage wallstat
*/

$site_url = home_url();
$theme_url = get_template_directory_uri();
$this_post_type = get_post_type( $post ); //表示されているページの投稿タイプを取得
$ancestor_info = get_ancestor_info($post); //先祖情報取得

?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1><?php the_title_attribute(); ?></h1>
		</div>
		<?php echo output_breadcrumb( $post, 'TOP' ); ?>
	</div>

	<article class="l-contents">
		<main class="l-main" role="main">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>
			<?php endwhile; ?>

			<!-- Wallstatとはページと子ページのみ表示-->
			<?php if($this_post_type == 'page' && $ancestor_info['slug'] == 'about' ): ?>
				<a  href="<?php echo home_url(); ?>/simulation/" class="p-about_banner">
					<img src="<?php echo $theme_url; ?>/assets/images/p-about_banner.png" width="" height="" alt="Wallstat（ウォールスタット）耐震シュミレーション">
				</a>
			<?php endif; ?>

		</main><!-- l-main END -->

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>
	</article><!-- l-contents END -->

<?php get_footer(); ?>
