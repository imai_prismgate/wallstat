<?php
/*
 * @package WordPress
 * @subpackage houserenova
 */
$theme_url = get_template_directory_uri();
$site_url  = home_url();
$this_post_type = get_post_type( $post ); //表示されているページの投稿タイプを取得
$ancestor_info = get_ancestor_info($post); //先祖情報取得
?>

<!-- 固定ページ_子ページリスト -->
<?php
	$get_post_ancestors = get_post_ancestors( $post->ID );
	$ancestor_info['id']  = array_pop( $get_post_ancestors );

	if($this_post_type == 'page' && $ancestor_info['id'] != 'null' ):
		echo output_side_childpages($post, false);
	endif;
?>
<!-- お知らせ・ブログ_カテゴリリスト・最新5件リスト -->
<?php
	if($this_post_type == 'blog'):
		echo side_categories( 'カテゴリ', 'blog_category' );
		// echo get_side_blog_list(5);
	endif;
?>

<!-- セミナー_カテゴリリスト・最新5件リスト -->
<?php
	if($this_post_type == 'seminar'):
		echo side_categories( 'カテゴリ', 'seminar_category' );
		// echo get_side_seminar_list(5);
	endif;
?>

<!-- バナー -->
<a class="p-side_banner" href="<?php echo home_url(); ?>/simulation/simulation_entry/">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/side_banner_simulation.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/side_banner_simulation@2x.png 2x" width="180" height="75" alt="耐震シミュレーションお申込み">
</a>
<a class="p-side_banner" href="https://k-forum.jp/" target="_blank">
	<img src="<?php echo get_template_directory_uri(); ?>/assets/images/side_banner_forum.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/side_banner_forum@2x.png 2x" width="180" height="75" alt="工務店フォーラム">
</a>