<?php
/*
 * @package WordPress
 * @subpackage houserenova
*/

$site_url = home_url();
$theme_url = get_template_directory_uri();
$ancestor_info = get_ancestor_info($post); //先祖情報取得
?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1><?php the_title_attribute(); ?></h1>
		</div>
		<?php echo output_breadcrumb( $post, 'TOP' ); ?>
	</div>

	<main class="l-contents">
		<article class="l-main" role="main">
		<?php if ( have_posts() ) : ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<?php
		$terms = get_the_terms( $post->ID, 'blog_category' );
			if ( !empty($terms) ) {
				if ( !is_wp_error( $terms ) ) {
					foreach( $terms as $term ) {
						$blog_category_link = get_term_link($term);
						$blog_category_slug = $term->slug;
						$blog_category_name = $term->name;
				}
			}
		}
		?>
		<div class="p-blog_single_box">
			<div class="p-blog_note">
				<p class="date"><?php the_time('Y.m.d'); ?></p>
				<p class="category cate_<?php echo $blog_category_slug; ?>"><span class="cate_<?php echo $blog_category_slug; ?>"><?php echo $blog_category_name; ?></span></p>
			</div>
			<div class="contents u-clearfix"><?php the_content(); ?></div>
		</div>

		<?php endwhile; ?>
		<?php endif; ?>

		<div class="p-single_footer">
			<ul class="p-single_footer_link">
			<?php previous_post_link('<li>%link</li>','← 前へ',false) ?>
				<li class="go_archive"><a href="../">一覧へ戻る</a></li>
			<?php next_post_link('<li>%link</li>','次へ →',false); ?>
			</ul>
		</div>
		</article><!-- l-main END -->

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>
	</main><!-- l-contents END -->

<?php get_footer(); ?>