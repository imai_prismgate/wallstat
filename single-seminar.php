<?php
/*
 * @package WordPress
 * @subpackage wallstat
 */

$site_url       = home_url();
$theme_url      = get_template_directory_uri();
$reserve_status = get_seminar_reserve_status($post);
$entry_info     = seminar_entry_info($post);
$ancestor_info = get_ancestor_info($post);
?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1><?php the_title_attribute(); ?></h1>
		</div>
		<?php echo output_breadcrumb( $post, 'TOP' ); ?>
	</div>

	<article class="l-contents">
		<main class="l-main" role="main">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<div class="p-seminar_single">

			<!-- アラートメッセージ -->
			<?php if ( $entry_info['head_message'] && !get_field('report') ) : ?>
			<p class="p-seminar_single__alert"><?php echo $entry_info['head_message']; ?></p>
			<?php endif; ?>

			<!-- セミナー詳細 -->
			<h2 class="p-h2"><?php the_title(); ?></h2>
			<div class="p-seminar_content p-h2_indent"><?php the_content(); ?></div>

			<?php if ( get_field('instructor') ) : ?>
			<h2 class="p-h2" style="margin-top: 2.5rem;">講師</h2>
			<div class="p-seminar_instructor p-h2_indent"><?php the_field('instructor'); ?></div>
			<?php endif; ?>

			<?php if ( get_field('instructor') ) : ?>
			<h2 class="p-h2" style="margin-top: 2.5rem;">内容</h2>
			<div class="p-seminar_content p-h2_indent"><?php the_field('content'); ?></div>
			<?php endif; ?>

			<!-- セミナー概要 -->
			<h2 class="p-h2" style="margin-top: 2.5rem;">概要</h2>
			<div class="p-h2_indent">
				<table class="c-table_base c-table_base--dott c-table_mobile_block">
					<tbody>
						<tr>
							<th>開催日時</th>
							<td>
								<span class="p-seminar_detail__date"><?php echo seminar_openday_string($post); ?></span><?php if ( get_field('date_time') ) : ?><span class="p-seminar_detail__time"><?php the_field('date_time'); ?></span><?php endif; ?>
							</td>
						</tr>
						<?php if ( get_field('price') ) : ?>
						<tr>
							<th>費用</th>
							<td><?php the_field('price'); ?>（税込）</td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('spot') ) : ?>
						<tr>
							<th>会場</th>
							<td><?php the_field('spot'); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('capacity') ) : ?>
						<tr>
							<th>定員</th>
							<td><?php the_field('capacity'); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('target_person') ) : ?>
						<tr>
							<th>対象の方</th>
							<td><?php the_field('target_person'); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('target_skill') ) : ?>
						<tr>
							<th>対象スキル</th>
							<td><?php the_field('target_skill'); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('belongings') ) : ?>
						<tr>
							<th>所持品</th>
							<td>
								<p><?php the_field('belongings'); ?></p>
								<a href="http://www.rish.kyoto-u.ac.jp/~nakagawa/" class="p-link_external"target="_blank">wallstatのダウンロードとインストールはこちらから </a>
						</td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('textbook') ) : ?>
						<tr>
							<th>教材</th>
							<td><?php the_field('textbook'); ?></td>
						</tr>
						<?php endif; ?>
						<?php if ( get_field('host') ) : ?>
						<tr>
							<th>主催</th>
							<td><?php the_field('host'); ?></td>
						</tr>
						<?php endif; ?>
						<tr>
							<th>お問い合わせ</th>
							<td>
								<p>一般社団法人工務店フォーラム事務局　<span class="tel telLink">TEL 048-951-2420</span></p>
								<p>または<a href="<?php echo home_url(); ?>/contact/">お問い合わせフォーム</a>でお問い合わせください。</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!-- ステータス判定によるメッセージ表示 -->
				<p class="p-seminar_entry_btn_wrap"><?php echo $entry_info['info_message']; ?></p>
			</div>

		</div><!-- p-seminar_single_box END -->
		<?php endwhile;?>

		</main><!-- l-main END -->

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>
	</article><!-- l-contents END -->

<?php get_footer(); ?>


