<?php
/*
 * @package WordPress
 * @subpackage houserenova
*/

$site_url = home_url();
$theme_url = get_template_directory_uri();
$ancestor_info = get_ancestor_info($post); //先祖情報取得
?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1><?php single_term_title(); ?></h1>
		</div>
		<?php echo output_breadcrumb( $post, 'TOP' ); ?>
	</div>

	<main class="l-contents">
		<article class="l-main" role="main">
		<?php if ( have_posts() ) : ?>
			<ul class="p-blog_archive_list">
				<?php while ( have_posts() ) : the_post(); ?>
				<?php
				$terms = get_the_terms( $post->ID, 'blog_category' );
					if ( !empty($terms) ) {
						if ( !is_wp_error( $terms ) ) {
							foreach( $terms as $term ) {
								$blog_category_link = get_term_link($term);
								$blog_category_slug = $term->slug;
								$blog_category_name = $term->name;
						}
					}
				}
				 ?>
				<?php $thumb_path = create_thumbnail_path($post, get_template_directory_uri().'/assets/images/p-blog_thumb.jpg'); ?>
				<li>
					<a href="<?php the_permalink(); ?>" class="p-blog_archive_list_thumb" style="background-image:url('<?php echo $thumb_path; ?>')"></a>
					<div class="p-blog_archive_list_info">
						<div class="p-blog_note">
							<p class="date"><?php the_time('Y.m.d'); ?></p>
							<p class="category cate_<?php echo $blog_category_slug; ?>"><span class="cate_<?php echo $blog_category_slug; ?>"><?php echo $blog_category_name; ?></span></p>
						</div>
						<a href="<?php the_permalink(); ?>" class="title"><?php the_title(); ?></a>
						<div class="text">
							<?php
							 if(mb_strlen($post->post_content,'UTF-8')>30):
								$content = str_replace('\n', '', mb_substr(strip_tags($post->post_content), 0, 30));
								$content.= ' ...';
								echo $content;
							else:
								echo str_replace('\n', '', strip_tags($post->post_content));
							endif;
							?>
						</div>
					</div>
				</li>
			<?php endwhile; ?>
			</ul>
		<?php else: ?>
			<p class="p-nodata">該当の記事はありません。</p>
		<?php endif; ?>

		<?php //pagenation
			if (function_exists("pagination")) {
				pagination($wp_query->max_num_pages);
			}
		?>
		</article><!-- l-main END -->

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>
	</main><!-- l-contents END -->

<?php get_footer(); ?>