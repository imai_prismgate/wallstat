<?php
/*
 * 無料相談・セミナー カテゴリごと一覧ページのテンプレートです
 * @package WordPress
 * @subpackage wallstat
 */

$site_url       = home_url();
$theme_url      = get_template_directory_uri();
$ancestor_info = get_ancestor_info($post);
$today    = date('Ymd');
$past_all = ( isset( $_GET['past'] ) ) ? $_GET['past'] : ''; //URLのパラメータを取得

//ターム情報を取得
 $taxonomy_name = 'seminar_category';
if( is_tax() ) {
	$this_wp_query  = $wp_query->query;
	$this_term_slug = $this_wp_query[$taxonomy_name];
	$term_info      =  get_term_by('slug', $this_term_slug, $taxonomy_name);
}
?>

<?php get_header(); ?>

	<div class="l-page_title">
		<div class="h1_box">
			<h1><?php echo $ancestor_info['title'] ?></h1>
		</div>
		<?php echo output_breadcrumb( $post, 'TOP' ); ?>
	</div>

	<article class="l-contents">
		<main class="l-main" role="main">

			<?php if ( $past_all != 'all' ): ?>
			<!-- これからのイベント -->
			<h2 class="p-h2" style="margin-bottom: 0;">開催予定のイベント・セミナー</h2>

			<?php
			if ( is_tax() ) { //カテゴリ一覧の場合
				$later_list = get_seminar_later_list('seminar', -1, 'seminar_category', $this_term_slug);
			} else { //それ以外のアーカイブ
				$later_list = get_seminar_later_list('seminar', -1);
			}
			if ( $later_list ) :
			?>

			<ul class="p-h2_indent p-seminar_archivelist is-later">
				<?php foreach ($later_list as $post) : ?>
				<li>
					<?php
					//申し込みステータス
					$entry_info = seminar_entry_info($post);
					if ( $entry_info['entry'] == true ){
						$class = 'is-ok';
					} elseif ( $entry_info['info_message'] == '参加申込受付前です。もうしばらくお待ちください。' ) {
						$class = 'is-before';
					} elseif ( $entry_info['info_message'] == '申し込みの必要はございません。当日、直接会場にお越しください。' ) {
						 $class = 'is-ok';
					} else {
						$class = 'is-end';
					}

					 ?>
					<a href="<?php the_permalink(); ?>" class="<?php echo $class; ?>">
						<div class="p-seminar_archivelist__info">
							<p class="p-seminar_archivelist__title"><?php the_title(); ?></p>
							<p class="p-seminar_archivelist__date">日時：<span class="p-seminar_detail__date"><?php echo seminar_openday_string($post); ?></span><?php if ( get_field('date_time') ) : ?><span class="p-seminar_detail__time"><?php the_field('date_time'); ?></span><?php endif; ?></p>
						</div>
					</a>
				</li>
				<?php endforeach; ?>
			</ul>
			<?php else: ?>
			<p class="p-nodata">これから開催されるイベント・セミナーはありません</p>
			<?php endif;?>
			<!-- /これからのイベント -->

			<?php endif; //if paged < 2 ?>

			<!-- 過去のイベント -->
			<h2 class="p-h2" style="margin-bottom: 0;">過去のイベント・セミナー</h2>
			<?php if ( have_posts() ) : ?>
			<ul class="p-h2_indent p-seminar_archivelist is-old">
				<?php while ( have_posts() ) : the_post(); ?>
				<li>
					<a href="<?php the_permalink(); ?>">
						<div class="p-seminar_archivelist__info">
							<p class="p-seminar_archivelist__title"><?php the_title(); ?></p>
							<p class="p-seminar_archivelist__date">日時：<span class="p-seminar_detail__date"><?php echo seminar_openday_string($post); ?></span><?php if ( get_field('date_time') ) : ?><span class="p-seminar_detail__time"><?php the_field('date_time'); ?></span><?php endif; ?></p>
						</div>
					</a>
				</li>
				<?php endwhile; ?>
			</ul>
			<?php if( $post->post_status == 'publish' && $past_all != 'all' ) : ?>
			<p class="u-mt_l p-seminar_archive__link_pastall u-ta_r"><a href="?past=all">過去のイベント・セミナー一覧 >></a></p>
			<?php endif; ?>
			<?php else: ?>
			<p class="p-nodata">過去のイベント・セミナーはありません</p>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
			<!-- /過去のイベント -->

			<!-- ページ送り -->
			<div class="l-main__footer p-seminar_archive__footer">
			 	<div class="p-seminar_archive__pager">
					<?php if ( $past_all == 'all' ): ?>
					<?php
					global $wp_rewrite;
					$paginate_base = get_pagenum_link(1);
					if(strpos($paginate_base, '?') || !$wp_rewrite->using_permalinks()){
						$paginate_format = '';
						$paginate_base = add_query_arg('paged', '%#%');
					} else {
						$paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '/').user_trailingslashit('page/%#%/', 'paged');
						$paginate_base .= '%_%';
					}
					echo paginate_links(array(
							'base' => $paginate_base,
							'format' => $paginate_format,
							'total' => $wp_query->max_num_pages,
							'mid_size' => 5,
							'current' => ($paged ? $paged : 1),
							'prev_text' => '&#171;',
							'next_text' => '&#187;',
							'type' => 'list',
						));
					 ?>
				 </div>
				<p class="l-main__go_back_page" style="margin-top:1rem;"><a href="<?php echo home_url('/seminar/'); ?>"><?php echo $ancestor_info['title']?>一覧へ</a></p>
			 	<?php elseif ( is_tax() ) : ?>
			 	<p class="l-main__go_back_page"><a href="../"><< <?php echo $ancestor_info['title']?>一覧へ</a></p>
				<?php  //else: ?>
				<!-- <p class="l-main__go_back_page"><a href="<?php echo home_url(); ?>">トップページへ</a></p> -->
				<?php endif; ?>
			</div>
		</main>

		<aside class="l-sidebar" role="complementary">
			<?php get_template_part('sidebar');?>
		</aside>

	</article><!-- l-contents END -->

<?php get_footer(); ?>